package com.microuhc.uhc.game;

import com.microuhc.uhc.MicroUHC;
import org.bukkit.Chunk;
import org.bukkit.block.Biome;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class OceanDisabler implements Listener{
    private final MicroUHC uhc;
    private final List<Biome> disabled = Arrays.asList(Biome.OCEAN, Biome.DEEP_OCEAN, Biome.FROZEN_OCEAN, Biome.BEACHES, Biome.COLD_BEACH, Biome.STONE_BEACH);
    private final Set<Integer> hashList = new HashSet<>();

    public OceanDisabler(MicroUHC uhc) {
        this.uhc = uhc;
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e){
        new BukkitRunnable(){
            public void run() {
                if (uhc.getGameState().equals(GameState.GENNING)) {
                    return;
                }
                disableBiomes(e.getChunk(), e.isNewChunk());
            }
        }.runTask(uhc);
    }

    public void disableBiomes(Chunk c, boolean isNew){
        int worldCordX = c.getX() << 4;
        int worldCordZ = c.getZ() << 4;
        boolean needregen = false;
        for(int x = 0; x < 16; x++){
            for(int z = 0; z < 16; z++){
                Biome biome = c.getWorld().getBiome(worldCordX + x, worldCordZ + z);
                if(disabled.contains(biome)){
                    c.getWorld().setBiome(worldCordX + x, worldCordZ + z, Biome.PLAINS);
                    needregen = true;
                }
            }
        }
        if(needregen && isNew && !Collections.synchronizedSet(hashList).contains(c.getX() * 31 + c.getZ())) {
            Collections.synchronizedSet(hashList).add(c.getX() * 31 + c.getZ());
            c.getWorld().regenerateChunk(c.getX(), c.getZ());
            System.out.println("Regenerated " + c.getX() + "," + c.getZ());
        }
    }
}
