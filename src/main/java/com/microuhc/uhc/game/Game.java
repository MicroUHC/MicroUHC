package com.microuhc.uhc.game;


import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.user.User;
import com.microuhc.uhc.util.PacketUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Game {

    private MicroUHC plugin;
    private ConfigManager configManager;

    private GameState gameState;
    private UUID host;
    private int slots;

    //game timer
    private Timer timer;

    public Game(MicroUHC plugin, ConfigManager configManager) {
        this.plugin = plugin;
        this.configManager = configManager;
    }

    /**
     * Setup the game
     */
    public void setup() {

        //attempt to recover game state
        gameState = GameState.fromId(configManager.getConfig().getInt("state", GameState.RESET.getId()));
        plugin.quickLog("Recovering with the gamestate " + gameState.getName() + " [" + gameState.getId() + "]");

        //attempt to set host
        if (gameState.isBefore(GameState.FINISHED) && gameState.isAfter(GameState.PREHOST)){
            host = UUID.fromString(configManager.getConfig().getString("host"));
            this.plugin.quickLog("Using host: " + host);
        } else {
            host = null;
        }

        //clear whitelist if needed
        new BukkitRunnable(){
            public void run() {
                if(gameState.is(GameState.RESET)){
                    Bukkit.getWhitelistedPlayers().clear();
                    Bukkit.setWhitelist(true);
                    gameState = GameState.PREHOST;
                }
            }
        }.runTask(plugin);

        //attempt to get slots
        slots = getSlots();
        plugin.quickLog("Using slots: " + slots);


        //set timer defaults
        setFinalHeal(10);
        setPvP(20);
        setBoarderShrink(65);

    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    /**
     * Get the world to be used for the game.
     *
     * @return The game world
     */
    public World getWorld() {
        World world;
        try {
            world = Bukkit.getWorld(configManager.getConfig().getStringList("world").get(0));
        } catch (IndexOutOfBoundsException e) {
            world = null;
        }
        return world;
    }

    /**
     * Get the world to be used for the game.
     *
     * @return The game world
     */
    public World getNether() {
        World world;
        try {
            world = Bukkit.getWorld(configManager.getConfig().getStringList("world").get(1));
        } catch (IndexOutOfBoundsException e) {
            world = null;
        }
        return world;
    }

    /**
     * Get all the worlds being used by the game.
     *
     * @return A list of game worlds.
     */
    public List<World> getWorlds() {
        List<World> worlds = new ArrayList<World>();

        for (String name : configManager.getConfig().getStringList("world")) {
            World world = Bukkit.getWorld(name);

            if (world != null) {
                worlds.add(world);

                World nether = Bukkit.getWorld(world.getName() + "_nether");
                World end = Bukkit.getWorld(world.getName() + "_end");

                if (nether != null) {
                    worlds.add(nether);
                }

                if (end != null) {
                    worlds.add(end);
                }
            }
        }

        return worlds;
    }

    /**
     * Get the current game state
     * @return the game state
     */
    public GameState getGameState() {
        return gameState;
    }

    /**
     * Get the current game host
     * @return the game host
     */
    public User getHost() {
        return host == null ? null : plugin.getUserMap().get(host);
    }

    /**
     * Get the game teamsize.
     *
     * @return The teamsize.
     */
    public String getTeamSize() {
        return configManager.getConfig().getString("teamsize", "No");
    }

    /**
     * sets the slots for the game
     * @param slots the ammount of slots for the game
     */
    public void setSlots(int slots) {
        this.slots = slots;

        //set slots and save config
        configManager.getConfig().set("slots", slots);
        configManager.saveConfig();
    }

    public int getSlots() {
        return configManager.getConfig().getInt("slots", 100);
    }

    /**
     * Sets the host for the game
     * @param host the host of the game
     */
    public void setHost(User host) {
        this.host = host.getUuid();
        for(User user : plugin.getUsers()){
            if(user.isOnline()) {
                PacketUtils.getInstance().updaterHeader(user);
            }
        }

        //set host and save config
        configManager.getConfig().set("host", host);
        configManager.saveConfig();
    }

    /**
     * Sets the host uuid
     * @param host the uuid of the host
     */
    public void setHostUUID(UUID host){
        this.host = host;
        for(User user : plugin.getUsers()){
            if(user.isOnline()) {
                PacketUtils.getInstance().updaterHeader(user);
            }
        }
    }

    /**
     * Sets the game state
     * @param gameState the game states
     */
    public void setGameState(GameState gameState) {
        this.gameState = gameState;

        //set state and save configss
        configManager.getConfig().set("state", gameState.getId());
        configManager.saveConfig();
    }

    /**
     * Set the world to be used for the game.
     * <p>
     * This will automaticly use all the same worlds
     * that has the world name with _end or _nether at the end.
     *
     * @param names The new world name.
     */
    public void setWorld(String... names) {
        //set world and save config
        configManager.getConfig().set("world", Arrays.asList(names));
        configManager.saveConfig();

        //update gui
        //gui.getGUI(GameInfoGUI.class).update();
    }

    /**
     * Set the matchpost for the game.
     *
     * @param matchpost The new matchpost.
     */
    public void setMatchPost(String matchpost) {
        //set matchpost and save config
        configManager.getConfig().set("matchpost", matchpost);
        configManager.saveConfig();


        //gui.getGUI(GameInfoGUI.class).update();
    }

    /**
     * Get the matchpost for the game.
     *
     * @return The game's matchpost.
     */
    public String getMatchPost() {
        return configManager.getConfig().getString("matchpost", "No_Post_Set");
    }

    /**
     * Set the time of the pvp for the game.
     *
     * @param pvp The time in minutes.
     */
    public void setPvP(int pvp) {
        configManager.getConfig().set("timer.pvp", pvp);
        configManager.saveConfig();

        timer.setPvP(pvp);
        //gui.getGUI(GameInfoGUI.class).update();
    }

    /**
     * Get the time when pvp will be enabled after the start.
     *
     * @return The time in minutes.
     */
    public int getPvP() {
        return configManager.getConfig().getInt("timer.pvp", 20);
    }

    /**
     * Set the time of the meetup of the game.
     *
     * @param meetup The time in minutes.
     */
    public void setMeetup(int meetup) {
        configManager.getConfig().set("timer.meetup", meetup);
        configManager.saveConfig();

        timer.setMeetup(meetup);
        //gui.getGUI(GameInfoGUI.class).update();
    }

    /**
     * Get the time when meetup will occur after the start.
     *
     * @return The time in minutes.
     */
    public int getMeetup() {
        return configManager.getConfig().getInt("timer.meetup", 90);
    }

    /**
     * Set the time the boarder starts shrinking to 100x100
     *
     * @param boarderShrink The time in minutes
     */
    public void setBoarderShrink(int boarderShrink) {
        configManager.getConfig().set("timer.boarderShrink", boarderShrink);
        configManager.saveConfig();

        timer.setBoarderShrink(boarderShrink);
    }

    /**
     * Get the time when boarder shrink will occur
     *
     * @return The time in minutes
     */
    public int getBoarderShrink() {
        return configManager.getConfig().getInt("timer.boarderShrink", 65);
    }

    /**
     * Set the time the for final heal
     *
     * @param finalHeal The time in minutes
     */
    public void setFinalHeal(int finalHeal) {
        configManager.getConfig().set("timer.finalHeal", finalHeal);
        configManager.saveConfig();

        timer.setFinalHeal(finalHeal);
    }

    /**
     * Get the time for final heal
     *
     * @return The time in minutes
     */
    public int getFinalHeal() {
        return configManager.getConfig().getInt("timer.finalHeal", 10);
    }
}
