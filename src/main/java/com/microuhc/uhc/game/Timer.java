package com.microuhc.uhc.game;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.user.User;
import com.microuhc.uhc.util.DateUtils;
import com.microuhc.uhc.util.EntityUtils;
import com.microuhc.uhc.util.PacketUtils;
import com.microuhc.uhc.util.PlayerUtils;
import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.WorldBorder;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Entity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

public class Timer {

    private MicroUHC uhc;
    private Game game;

    public Timer (MicroUHC uhc, Game game) {
        this.uhc = uhc;
        this.game = game;
    }


    private BukkitRunnable taskSeconds;
    private BukkitRunnable taskMinutes;

    private int timePassedSeconds;
    private int timePassed;

    /**
     * Set the time since the game started.
     *
     * @param time The new time.
     */
    public void setTimePassed(int time) {
        this.timePassedSeconds = (time * 60);
        this.timePassed = time;
    }

    /**
     * Get the time since the game started.
     *
     * @return The time.
     */
    public int getTimePassed() {
        return timePassed;
    }

    /**
     * Get the time in seconds since the game started.
     *
     * @return The time in seconds.
     */
    public int getTimePassedSeconds() {
        return timePassedSeconds;
    }

    private int pvpSeconds;
    private int pvp;

    /**
     * Set the time until pvp enables.
     *
     * @param pvp The new time.
     */
    public void setPvP(int pvp) {
        this.pvpSeconds = (pvp * 60);
        this.pvp = pvp;
    }

    /**
     * Get the time until pvp enables.
     *
     * @return The time.
     */
    public int getPvP() {
        return pvp;
    }

    /**
     * Get the time in seconds until pvp enables.
     *
     * @return The time in seconds.
     */
    public int getPvPSeconds() {
        return pvpSeconds;
    }

    private int meetupSeconds;
    private int meetup;

    /**
     * Set the time until meetup.
     *
     * @param meetup The new time.
     */
    public void setMeetup(int meetup) {
        this.meetupSeconds = (meetup * 60);
        this.meetup = meetup;
    }

    /**
     * Get the time until meetup.
     *
     * @return The time.
     */
    public int getMeetup() {
        return meetup;
    }

    /**
     * Get the time in seconds until meetup.
     *
     * @return The time in seconds.
     */
    public int getMeetupSeconds() {
        return meetupSeconds;
    }

    private int boarderShrinkSeconds;
    private int boarderShrink;

    public void setBoarderShrink(int boarderShrink) {
        this.boarderShrinkSeconds = (boarderShrink * 60);
        this.boarderShrink = boarderShrink;
    }

    /**
     * Get the time until boarder shrink
     *
     * @return The time.
     */
    public int getBoarderShrink() { return boarderShrink; }

    /**
     * Get the time in seconds until boarder shrink
     *
     * @return The time in seconds
     */
    public int getBoarderShrinkSeconds() { return boarderShrinkSeconds; }

    private int finalHealSeconds;
    private int finalHeal;

    /**
     * Set the time until final heal.
     *
     * @param finalHeal The new time.
     */
    public void setFinalHeal(int finalHeal) {
        this.finalHealSeconds = (finalHeal * 60);
        this.finalHeal = finalHeal;
    }

    /**
     * Get the time until final heal.
     *
     * @return The time.
     */
    public int getFinalHeal() {
        return finalHeal;
    }

    /**
     * Get the time in seconds until meetup.
     *
     * @return The time in seconds.
     */
    public int getFinalHealSeconds() {
        return finalHealSeconds;
    }

    public void start() {
        for (int i = 0; i < 150; i++) {
            Bukkit.getOnlinePlayers().forEach(online -> online.sendMessage("§0"));
        }

        MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.WHITE + "The game will start in " +
        ChatColor.GREEN + " 30" + ChatColor.WHITE + " seconds");
        MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

        new BukkitRunnable(){

            private int startingTime = 31;
            private int i = 0;

            public void run() {
                startingTime--;

                if (startingTime > 0) {
                    MicroUHC.getInstance().broadcast(BarColor.GREEN,
                            ChatColor.WHITE + "Game is starting in §8» §a" + startingTime
                            + ChatColor.WHITE + " seconds");

                    if (startingTime <= 5) {
                        MicroUHC.getInstance().broadcast(Sound.ENTITY_ARROW_HIT);
                    }
                }

                switch (startingTime) {
                    case 25:

                        break;
                    case 20:
                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " Use §a/scen §7to view scenario information about this game.");
                        break;
                    case 15:
                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " If you want to find the matchpost, use §a/post");
                        break;
                    case 10:
                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " If you have any questions, use §a/uhc§7! If that didn't help ask us with §a/helpop§7!");
                        break;
                    case 5:
                        MicroUHC.getInstance().broadcast(BarColor.GREEN,
                                ChatColor.WHITE + "Game is starting in §8» §45 " + ChatColor.WHITE + "seconds");
                        break;
                    case 4:
                        MicroUHC.getInstance().broadcast(BarColor.GREEN,
                                ChatColor.WHITE + "Game is starting in §8» §c4 " + ChatColor.WHITE + "seconds");
                        break;
                    case 3:
                        MicroUHC.getInstance().broadcast(BarColor.GREEN,
                                ChatColor.WHITE + "Game is starting in §8» §63 " + ChatColor.WHITE + "seconds");
                        break;
                    case 2:
                        MicroUHC.getInstance().broadcast(BarColor.GREEN,
                                ChatColor.WHITE + "Game is starting in §8» §e2 " + ChatColor.WHITE + "seconds");
                        break;
                    case 1:
                        MicroUHC.getInstance().broadcast(BarColor.GREEN,
                                ChatColor.WHITE + "Game is starting in §8» §a1 " + ChatColor.WHITE + "second");
                        break;
                    case 0:
                        PlayerUtils.broadcast("§8» §m-----------------------------------§8 «");
                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " The game has now started!");
                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " Good luck, Have fun!");
                        PlayerUtils.broadcast("§8» §m-----------------------------------§8 «");

                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " PvP will be enabled in: §a" + game.getPvP() + "§f minutes.");
                        PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + "Final heal is in: §a " + game.getFinalHeal() + "§f minutes");
                        /*if (scen.getScenario(DragonRush.class).isEnabled()) {
                            PlayerUtils.broadcast(Main.PREFIX + "The dragon wins after: §a" + game.getMeetup() + " minutes.");
                        }else if (scen.getScenario(SuddenDeath.class).isEnabled()) {
                            PlayerUtils.broadcast(Main.PREFIX + "Sudden Death is in: §a" + game.getMeetup() + " minutes.");
                        } else {
                            PlayerUtils.broadcast(Main.PREFIX + "Meetup is in: §a" + game.getMeetup() + " minutes.");
                        }*/


                        PlayerUtils.broadcast("§8» §m-----------------------------------§8 «");

                        //Bukkit.getPluginManager().registerEvents(spec.getSpecInfo(), plugin);

                        //game.setState(State.INGAME);


                        meetup = game.getMeetup();
                        pvp = game.getPvP();
                        boarderShrink = game.getBoarderShrink();
                        finalHeal = game.getFinalHeal();

                        meetupSeconds = (meetup * 60);
                        pvpSeconds = (pvp * 60);
                        boarderShrinkSeconds = (boarderShrink * 60);
                        finalHealSeconds = (finalHeal * 60);


                        //todo: game scoreboard

                        Bukkit.setIdleTimeout(10);
                        timer();

                        /*if (game.isRecordedRound() || game.isPrivateGame()) {
                            game.setMuted(false);
                        }*/

                        game.getWorlds().forEach(world -> {
                            world.setDifficulty(Difficulty.HARD);
                            world.setPVP(false);
                            world.setTime(0);

                            world.setGameRuleValue("doDaylightCycle", "true");
                            world.setThundering(false);
                            world.setStorm(false);

                            world.setSpawnFlags(false, true);

                            world.getEntities().stream()
                                    .filter(mob -> EntityUtils.isButcherable(mob.getType()))
                                    .forEach(Entity::remove);
                        });

                        //SlaveMarket market = scen.getScenario(SlaveMarket.class);
                        //Kings kings = scen.getScenario(Kings.class);

                        /*game.getOfflinePlayers().forEach(offline -> {
                            try {
                                plugin.getUser(offline).increaseStat(Stat.GAMESPLAYED);
                            } catch (InvalidUserException ignored) {}
                        });*/


                        Bukkit.getOnlinePlayers().forEach(online -> {
                            //PacketUtils.sendAction(online, "§7Final heal is given in §8» §a" + DateUtils.secondsToString(getFinalHealSeconds()));
                            /*uhc.getAnnounceBar().setTitle("§7Final heal is given in §8» §a" + DateUtils.secondsToString(getFinalHealSeconds()));
                            uhc.getAnnounceBar().setColor(BarColor.GREEN);

                            uhc.getAnnounceBar().setProgress((float)i/(float)getFinalHealSeconds());
                            uhc.getAnnounceBar().setVisible(true);
                            i++;*/
                            PacketUtils.getInstance().sendAction(online, "§7Final heal is given in §8» §a" + DateUtils.secondsToString(getFinalHealSeconds()));

                            MicroUHC.getInstance().broadcast(Sound.ENTITY_ARROW_HIT);

                           // online.playSound(online.getLocation(), Sound.ENTITY_ARROW_HIT, 1, 1);

                            online.awardAchievement(Achievement.OPEN_INVENTORY);
                            online.removeAchievement(Achievement.MINE_WOOD);


                            //spec.getSpecInfo().getTotal(online).clear();

                            User user = uhc.getUserMap().get(online.getUniqueId());
                            user.resetEffects();
                            user.resetHealth();
                            user.resetFood();
                            user.resetExp();

                            if (!user.getRole().equals(GameRole.PLAYER)) {
                                //PacketUtils.sendTitle(online, "§aGo!", "§7Have fun spectating!", 5, 40, 25);

                                if (online.getGameMode() != GameMode.SPECTATOR) {
                                    online.setGameMode(GameMode.SPECTATOR);
                                }

                                online.sendMessage(MicroUHC.PREFIX + "Your spec info has been enabled!");
                            } else {
                                //PacketUtils.sendTitle(online, "§aGo!", "§7Good luck & Have fun!", 5, 40, 25);

                                if (online.getGameMode() != GameMode.SURVIVAL) {
                                    online.setGameMode(GameMode.SURVIVAL);
                                }

                                /*if (kings.isEnabled() && kings.getKings().contains(online.getName())) { // kings effects should not be cleared.
                                    online.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, NumberUtils.TICKS_IN_999_DAYS, 0));
                                    online.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, NumberUtils.TICKS_IN_999_DAYS, 0));
                                    online.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, NumberUtils.TICKS_IN_999_DAYS, 0));
                                    online.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, NumberUtils.TICKS_IN_999_DAYS, 0));
                                } else {
                                    online.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 100));
                                }*/

                                online.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 100));

                                /*if (market.isEnabled() && market.owners.contains(online.getName())) { // slave market owner has diamonds that could be used ingame
                                    PlayerInventory inv = online.getInventory();

                                    for (ItemStack item : inv.getContents()) {
                                        if (item == null) {
                                            continue;
                                        }

                                        if (item.getType() == Material.DIAMOND) {
                                            continue;
                                        }

                                        inv.removeItem(item);
                                    }

                                    inv.setArmorContents(null);
                                    online.setItemOnCursor(new ItemStack(Material.AIR));

                                    InventoryView openInv = online.getOpenInventory();

                                    if (openInv.getType() == InventoryType.CRAFTING) {
                                        openInv.getTopInventory().clear();
                                    }
                                } else {
                                    user.resetInventory();
                                    user.giveStarterFood();
                                }*/

                                //reset inventory and give starter food
                                user.resetInventory();
                                user.giveStarterFood();
                            }
                        });

                        //Bukkit.getPluginManager().callEvent(new GameStartEvent()); // call the game start event after the potion effects, inventory clearing and setting up is done
                        break;
                }
            }

        }.runTaskTimer(uhc, 0, 20);
    }

    public void timer() {
        stopTimers();

        taskMinutes = new BukkitRunnable() {
            public void run() {
                timePassed++;
                pvp--;
                meetup--;
                boarderShrink--;
                finalHeal--;

                if (timePassed == 2) {
                    game.getWorlds().forEach(world -> world.setSpawnFlags(true, true));

                    MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.WHITE + "Monsters will now start spawning.");
                    MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);
                }

                if (pvp == 0) {
                    MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.WHITE + "PvP/iPvP has been enabled.");

                    //MicroUHC.getInstance().broadcast(BarColor.GREEN, "§4PvP has been enabled!");
                    MicroUHC.getInstance().broadcast(Sound.ENTITY_ENDERDRAGON_GROWL);

                    //Bukkit.getOnlinePlayers().forEach(online -> PacketUtils.sendTitle(online, "", "§4PvP has been enabled!", 5, 40, 25));
                    //MicroUHC.getInstance().broadcast(Sound.ENTITY_ENDERDRAGON_GROWL);

                    game.getWorlds().forEach(world -> world.setPVP(true));
                    //Bukkit.getPluginManager().callEvent(new PvPEnableEvent());
                }

                if (boarderShrink == 0) {

                    game.getWorlds().forEach(world -> world.setThundering(false));
                    game.getWorlds().forEach(world -> world.setStorm(false));


                    game.getWorlds().forEach(world -> world.setGameRuleValue("doDaylightCycle", "false"));
                    game.getWorlds().forEach(world -> world.setTime(6000));

                    MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.WHITE + "Permaday has been activated.");
                    MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

                    /*if (!scen.getScenario(Astrophobia.class).isEnabled() && !scen.getScenario(Permanight.class).isEnabled()) {
                        game.getWorlds().forEach(world -> world.setGameRuleValue("doDaylightCycle", "false"));
                        game.getWorlds().forEach(world -> world.setTime(6000));

                        PlayerUtils.broadcast(Main.PREFIX + "Permaday has been activated.");
                    }*/

                    //Bukkit.getPluginManager().callEvent(new BoarderShrinkEvent());
                }


                if (pvp > 0) {
                    String pvpToString = String.valueOf(pvp);

                    if (pvpToString.equals("1") || pvpToString.endsWith("5") || pvpToString.endsWith("0")) {
                        PlayerUtils.broadcast(MicroUHC.PREFIX + "PvP will be enabled in §a" + DateUtils.advancedTicksToString(pvp * 60) + "§7.");
                        //PlayerUtils.broadcast(Main.PREFIX + "Reminder: Stalking is " + feat.getFeature(StalkingFeature.class).getStalkingRule().getMessage().toLowerCase() + "§7.");

                        if (pvp != 0) {
                            MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_PLING);
                        }
                    }
                }


            }
        };

        taskMinutes.runTaskTimer(uhc, 1200, 1200);

        taskSeconds = new BukkitRunnable() {
            public void run() {
                timePassedSeconds++;
                pvpSeconds--;
                meetupSeconds--;
                boarderShrinkSeconds--;
                finalHealSeconds--;

                uhc.getAnnounceBar().setVisible(true);

                if (finalHealSeconds == 0) {
                    MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.WHITE + "Final heal has been given.");
                    MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

                    uhc.getAnnounceBar().setVisible(false);

                    Bukkit.getOnlinePlayers().forEach(online -> {
                        /*if (game.isPrivateGame() || game.isRecordedRound()) {
                            PacketUtils.sendTitle(online, "§6Final heal!", "", 5, 40, 10);
                        } else {
                            PacketUtils.sendTitle(online, "§6Final heal!", "§7Do not ask for another one", 5, 40, 25);
                        }*/
                        //PacketUtils.sendTitle(online, "§6Final heal!", "§7Do not ask for another one", 5, 40, 25);

                        User user = uhc.getUserMap().get(online.getUniqueId());
                        user.resetHealth();
                        user.resetFood();

                        online.setFireTicks(0);
                    });

                    //Bukkit.getPluginManager().callEvent(new FinalHealEvent());
                }

                /*if (finalHealSeconds == 0 && pvpSeconds > 0) {
                    uhc.getAnnounceBar().setTitle("§7PvP enabled in §8» §a" + DateUtils.secondsToString(finalHealSeconds));
                    uhc.getAnnounceBar().setColor(BarColor.GREEN);

                    uhc.getAnnounceBar().setProgress((float)pvpSeconds/(float)(60 * game.getPvP()));
                }*/

                if (finalHealSeconds > 0) {
                    //Bukkit.getOnlinePlayers().forEach(online -> PacketUtils.getInstance().sendAction(online, "§7Final heal is given in §8» §a" + DateUtils.secondsToString(finalHealSeconds)));

                    uhc.getAnnounceBar().setTitle("§7Final heal is given in §8» §a" + DateUtils.secondsToString(finalHealSeconds));
                    uhc.getAnnounceBar().setColor(BarColor.GREEN);

                    uhc.getAnnounceBar().setProgress((float)finalHealSeconds/(float)(60 * game.getFinalHeal()));
                    //uhc.getAnnounceBar().setVisible(true);
                }


            }
        };

        taskSeconds.runTaskTimer(uhc, 20, 20);
    }

    /**
     * Stop all the running timers.
     */
    public void stopTimers() {
        BukkitScheduler scheduler = Bukkit.getScheduler();

        if (taskMinutes != null && (scheduler.isQueued(taskMinutes.getTaskId()) || scheduler.isCurrentlyRunning(taskMinutes.getTaskId()))) {
            taskMinutes.cancel();
        }

        if (taskSeconds != null && (scheduler.isQueued(taskSeconds.getTaskId()) || scheduler.isCurrentlyRunning(taskSeconds.getTaskId()))) {
            taskSeconds.cancel();
        }
    }

}
