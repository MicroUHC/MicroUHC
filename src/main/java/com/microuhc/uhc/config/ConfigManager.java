package com.microuhc.uhc.config;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.util.FileUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import ru.tehkode.permissions.backends.file.FileConfig;

import java.io.File;

public class ConfigManager {

    @Getter
    private static ConfigManager instance = new ConfigManager();

    //messages
    private FileConfiguration messages;
    private File messagesFile;

    //settings
    private FileConfiguration settings;
    private File settingsFile;

    //world
    private FileConfiguration worlds;
    private File worldsFile;

    //config
    private FileConfiguration config;
    private File configFile;

    //data
    private static FileConfiguration data;
    private File dataFile;

    private ConfigManager() {

    }

    public void setup() {

        //log setup of ConfigManager
        MicroUHC.getInstance().getLogger().info("Loading database configurations");

        //make sure data folders exist, create if they don't exist.
        if (!MicroUHC.getInstance().getDataFolder().exists()){
            try {
                MicroUHC.getInstance().getDataFolder().mkdir();
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Error creating data folder: " + ex.getMessage());
            }
        }

        //make sure our config files exist, create if they don't exists
        configFile = new File(MicroUHC.getInstance().getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Error creating config file: " + ex.getMessage());
            }
        }

        settingsFile = new File(MicroUHC.getInstance().getDataFolder(), "settings.yml");
        if (!settingsFile.exists()) {
            try {
                settingsFile.createNewFile();
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Error creating config file: " + ex.getMessage());
            }
        }

        worldsFile = new File(MicroUHC.getInstance().getDataFolder(), "worlds.yml");
        if (!worldsFile.exists()) {
            try {
                worldsFile.createNewFile();
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Error creating config file: " + ex.getMessage());
            }
        }

        messagesFile = new File(MicroUHC.getInstance().getDataFolder(), "messages.yml");
        if (!messagesFile.exists()) {
            try {
                FileUtils.loadFile("messages.yml");
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Error creating config file: " + ex.getMessage());
            }
        }

        dataFile = new File(MicroUHC.getInstance().getDataFolder(), "data.yml");
        if (!dataFile.exists()) {
            try{
                dataFile.createNewFile();
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Error creating config file: " + ex.getMessage());
            }
        }


        //load config files
        try {

            config = new YamlConfiguration();
            config.load(configFile);

            settings = new YamlConfiguration();
            settings.load(settingsFile);

            worlds = new YamlConfiguration();
            worlds.load(worldsFile);

            messages = new YamlConfiguration();
            messages.load(messagesFile);

            data = new YamlConfiguration();
            data.load(dataFile);

        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error loading configs: " + ex.getMessage());
        }

        MicroUHC.getInstance().getLogger().info("Database configurations loaded successfully!");
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public FileConfiguration getSettings() {
        return settings;
    }

    public FileConfiguration getWorlds() {
        return worlds;
    }

    public FileConfiguration getMessages() {
        return messages;
    }

    public static FileConfiguration getData() {
        return data;
    }

    public void saveConfig() {
        try {
            config.save(configFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error saving config: " + ex.getMessage());
        }
    }

    public void saveWorlds() {
        try {
            worlds.save(worldsFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error saving worlds: " + ex.getMessage());
        }
    }

    public void saveSettings() {
        try {
            settings.save(settingsFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error saving settings: " + ex.getMessage());
        }
    }

    public void saveMessages() {
        try {
            messages.save(messagesFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error saving messages: " + ex.getMessage());
        }
    }

    public void saveData() {
        try {
            data.save(dataFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error saving data: " + ex.getMessage());
        }
    }

    public boolean isDebugEnabled() {
        return getConfig().getBoolean("debug", false);
    }

    public void reloadConfig() {
        try {
            config = new YamlConfiguration();
            config.load(configFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error loading config: " + ex.getMessage());
        }
    }

    public void reloadWorlds() {
        try {
            worlds = new YamlConfiguration();
            worlds.load(worldsFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error loading worlds: " + ex.getMessage());
        }
    }

    public void reloadMessages() {
        try {
            messages = new YamlConfiguration();
            messages.load(messagesFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error loading messages: " + ex.getMessage());
        }
    }

    public void reloadData() {
        try {
            data = new YamlConfiguration();
            data.load(dataFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Error loading data: " + ex.getMessage());
        }
    }

}
