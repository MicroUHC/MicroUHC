package com.microuhc.uhc.config;

import java.util.HashMap;
import java.util.logging.Logger;

import com.microuhc.uhc.util.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) Microcraft MC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: com.microcraftmc.api
 * Project: MicroAPI
 *
 */

public class MessageManager {

    private static MessageManager instance = new MessageManager();
    private static ConfigManager configManager = ConfigManager.getInstance();
    private HashMap<PrefixType, String>prefix = new HashMap<PrefixType, String>();

    public static MessageManager getInstance() {
        return instance;
    }

    public void setup(){
        FileConfiguration f = configManager.getMessages();
        prefix.put(PrefixType.Main, MessageUtils.replaceColors(f.getString("prefix.main")));
        prefix.put(PrefixType.Alert, MessageUtils.replaceColors(f.getString("prefix.alert")));
        prefix.put(PrefixType.Whitelist, MessageUtils.replaceColors(f.getString("prefix.whitelist")));
        prefix.put(PrefixType.Permissions, MessageUtils.replaceColors(f.getString("prefix.permissions")));
        prefix.put(PrefixType.Vote, MessageUtils.replaceColors(f.getString("prefix.vote")));
        prefix.put(PrefixType.Arrow, MessageUtils.replaceColors(f.getString("prefix.arrow")));
    }

    /**
     * SendMessage
     *
     * Loads a Message from messages.yml, converts its colors and replaces vars in the form of {$var} with its correct values,
     * then sends to the player, adding the correct prefix
     *
     * @param type
     * @param input
     * @param sender
     * @param args
     */
    public void sendFormattedMessage(PrefixType type, String input, CommandSender sender, String ... args) {
        String msg = configManager.getMessages().getString("messages." + input);
        boolean enabled = configManager.getMessages().getBoolean("messages."+input+"_enabled", true);

        if(msg == null){
            sender.sendMessage(ChatColor.RED+"Failed to load message for messages."+input);
            return;
        }

        if(!enabled) {
            return;
        }

        if(args != null && args.length != 0){
            msg = MessageUtils.replaceVars(msg, args);
        }

        msg = MessageUtils.replaceColors(msg);
        sender.sendMessage(prefix.get(type) + " " + ChatColor.RESET + msg);
    }

    /**
     * SendMessage
     *
     * Sends a pre formated message from the plugin to a player, adding correct prefix first
     *
     * @param type
     * @param msg
     * @param sender
     */

    public void sendPrefixMessage(PrefixType type, String msg, CommandSender sender){
        sender.sendMessage(prefix.get(type) + " " + ChatColor.RESET + msg );
    }

    public void broadcastFormattedMessage(PrefixType type, String input, String ...args ) {
        String msg = configManager.getMessages().getString("messages." + input);
        boolean enabled = configManager.getMessages().getBoolean("messages."+input+"_enabled", true);

        if(msg == null){
            Bukkit.broadcastMessage(ChatColor.RED+"Failed to load message for messages."+input);
            return;
        }

        if(!enabled) {
            return;
        }

        if(args != null && args.length != 0){
            msg = MessageUtils.replaceVars(msg, args);
        }
        msg = MessageUtils.replaceColors(msg);
        Bukkit.broadcastMessage(prefix.get(type)+ " "  + ChatColor.RESET + msg);
    }

    public void broadcastMessage(PrefixType type, String msg, String permission) {
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (permission != null && !online.hasPermission(permission)) {
                continue;
            }

            online.sendMessage(getPrefix(type) + msg);
        }
    }

    public void broadcastMessage(PrefixType type, String msg) {
        Bukkit.broadcastMessage(getPrefix(type) + msg);
    }

    public String getPrefix(PrefixType type) {
        return prefix.get(type) + " " + ChatColor.RESET;
    }

}
