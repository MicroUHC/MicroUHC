package com.microuhc.uhc.config;

/**
 * Copyright Statement
 * ----------------------
 * Copyright (C) IgniteUHC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Class information
 * ---------------------
 * Package: com.microuhc.uhc
 * Project: IgniteUHC
 *
 */

public enum PrefixType {

    Main,
    Alert,
    Whitelist,
    Vote,
    Permissions,
    Arrow

}
