package com.microuhc.uhc.user;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerListHeaderFooter;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.game.GameRole;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * The Bubble Network 2016
 * PolarUHC
 * 7/26/2016 {9:02 PM}
 * Created July 2016
 */
public class User {
    private final UUID uuid;
    private String name = null;
    private Player player = null;
    private PermissionUser permissionUser = null;
    private GameRole role = GameRole.PLAYER;
    private BossBar bossBar = Bukkit.createBossBar("", BarColor.PURPLE, BarStyle.SOLID);;
    private BukkitRunnable bossBarCallBack = null;

    private FileConfiguration config;
    private File userFile;
    private File userFolder = new File(MicroUHC.getInstance().getDataFolder() + File.separator + "users" + File.separator);

    private boolean creating = false;

    public User(UUID uuid) {
        this.uuid = uuid;

        if (!userFolder.exists()) {
            userFolder.mkdir();
        }

        //check if user file exists
        userFile = new File(userFolder, uuid + ".yml");
        if (!userFile.exists()) {
            try {
                userFile.createNewFile();
                creating = true;
            } catch (Exception ex) {
                MicroUHC.getInstance().getLogger().severe("Could not create data file: " + ex.getMessage());
            }
        }

        //load users config
        try {
            config = new YamlConfiguration();
            config.load(userFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Could not load user data file: " + ex.getMessage());
        }

        //do we setup the user?
        if (creating) {
            player = Bukkit.getPlayer(uuid);

            if (player != null) {
                config.set("username", player.getName());
                config.set("uuid", player.getUniqueId());
                config.set("ip", player.getAddress().getAddress().getHostAddress());
            }

            config.set("firstjoined", new Date().getTime());
            config.set("lastlogin", new Date().getTime());
            config.set("lastlogout", -1l);

            config.set("muted.status", false);
            config.set("muted.reason", "NOT_MUTED");
            config.set("muted.time", -1);

            saveConfig();
        }

        bossBar.setVisible(false);
    }

    public String getMessagePrefix(){

        /*switch (role) {
            case HOST:
                return role.getPrefix() + getName();
            case MOD:
                return role.getPrefix() + getName();
            case SPECTATOR:
                return role.getPrefix() + getName();
            default:
                return getName();
        }*/

        if (role == GameRole.HOST) {
            return role.getPrefix() + getName();
        }

        if (role == GameRole.MOD) {
            return role.getPrefix() + getName();
        }

        if (role == GameRole.SPECTATOR) {
            return role.getPrefix() + getName();
        }

        return getPexPrefix() + getName() + getPexSuffix();
    }

    public void setRole(GameRole role) {
        this.role = role;
    }

    public Player getPlayer() {
        return player;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setPlayer(Player player) {
        this.bossBar.addPlayer(player);
        this.player = player;
        this.permissionUser = PermissionsEx.getPermissionManager().getUser(player);
        this.name = player.getName();
    }

    public void cleanUp(){
        this.bossBar.removePlayer(player);
    }

    public PermissionUser getPermissionUser() {
        return permissionUser;
    }

    public void setPermissionUser(PermissionUser permissionUser) {
        this.permissionUser = permissionUser;
        this.player = permissionUser.getPlayer();
        this.name = player.getName();
    }

    public GameRole getRole(){
        return role;
    }

    public boolean isOnline(){
        return player != null && player.isOnline();
    }

    public boolean needsScatter(){
        return role == GameRole.PLAYER;
    }

    public boolean isStaff(){
        return role == GameRole.HOST || role == GameRole.MOD;
    }

    public boolean bypassWhitelist(){
        return role != GameRole.PLAYER;
    }

    public boolean bypassFull(){
        return role != GameRole.PLAYER || (permissionUser != null && permissionUser.has("uhc.joinfull"));
    }

    public void sendBossBar(MicroUHC uhc, BarColor barColor, String message){
        if(bossBarCallBack != null){
            bossBarCallBack.cancel();
        }
        bossBar.setTitle(message);
        bossBar.setProgress(0.0);
        bossBar.setVisible(true);
        bossBar.setColor(barColor);
        bossBarCallBack = new BukkitRunnable() {
            final double num = 0.025;
            double sofar = 0;
            public void run() {
                if((sofar += num) > 1){
                    cancel();
                    bossBar.setVisible(false);
                    bossBarCallBack = null;
                }
                else bossBar.setProgress(sofar);
            }
        };
        bossBarCallBack.runTaskTimerAsynchronously(uhc, 0, 1);
    }

    public void play(Sound sound){
        player.playSound(player.getLocation(), sound, 1f, 1f );
    }

    public boolean canChangeRole(GameRole gameRole){
        return hasGroupPerm(gameRole.getPermissionNeeded());
    }

    public boolean hasGroupPerm(String perm){
        for(PermissionGroup permissionGroup: permissionUser.getParents()){
            if(permissionGroup.has(perm)) return true;
        }
        return true;
    }

    public String getPexPrefix(){
        return permissionUser.getPrefix() == null ? "" : ChatColor.translateAlternateColorCodes('&', permissionUser.getPrefix());
    }
	
    public String getPexSuffix(){
        return permissionUser.getSuffix() == null ? "" : ChatColor.translateAlternateColorCodes('&', permissionUser.getSuffix());
    }

    public void setHeaderFooter(MicroUHC uhc, String headerstring, String footerstring){
        WrapperPlayServerPlayerListHeaderFooter playServerPlayerListHeaderFooter = new WrapperPlayServerPlayerListHeaderFooter();
        playServerPlayerListHeaderFooter.setHeader(WrappedChatComponent.fromText(headerstring));
        playServerPlayerListHeaderFooter.setFooter(WrappedChatComponent.fromText(footerstring));
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, playServerPlayerListHeaderFooter.getHandle());
        } catch (Throwable e1) {
            uhc.exceptionLog(e1, "Could not send tab packet");
        }
    }

    /**
     * Reset the players effects.
     */
    public void resetEffects() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return;
        }

        Collection<PotionEffect> effects = player.getActivePotionEffects();

        for (PotionEffect effect : effects) {
            player.removePotionEffect(effect.getType());
        }
    }

    /**
     * Reset the players health.
     */
    public void resetHealth() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return;
        }

        player.setHealth(player.getMaxHealth());
    }

    /**
     * Reset the players food.
     */
    public void resetFood() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return;
        }

        player.setSaturation(5.0F);
        player.setExhaustion(0F);
        player.setFoodLevel(20);
    }

    /**
     * Reset the players xp.
     */
    public void resetExp() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return;
        }

        player.setTotalExperience(0);
        player.setLevel(0);
        player.setExp(0F);
    }

    /**
     * Reset the players inventory.
     */
    public void resetInventory() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return;
        }

        PlayerInventory inv = player.getInventory();

        inv.clear();
        inv.setArmorContents(null);
        player.setItemOnCursor(new ItemStack(Material.AIR));

        InventoryView openInventory = player.getOpenInventory();

        if (openInventory.getType() == InventoryType.CRAFTING) {
            openInventory.getTopInventory().clear();
        }
    }

    /**
     * Give the player their starer food
     */
    public void giveStarterFood() {
        Player player = Bukkit.getPlayer(uuid);

        if (player == null) {
            return;
        }

        PlayerInventory inv = player.getInventory();
        ItemStack starterFood = new ItemStack(Material.COOKED_BEEF, 10);

        inv.setItem(0, starterFood);
    }

    public void saveConfig() {
        try {
            config.save(userFile);
        } catch (Exception e) {
            MicroUHC.getInstance().getLogger().severe(ChatColor.RED + "Could not save " + userFile.getName() + "!");
        }

        reloadConfig();
    }

    public void reloadConfig() {
        try {
            config = new YamlConfiguration();
            config.load(userFile);
        } catch (Exception ex) {
            MicroUHC.getInstance().getLogger().severe("Could not load user data file: " + ex.getMessage());
        }
    }
}
