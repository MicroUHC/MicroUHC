package com.microuhc.uhc;

import com.comphenix.protocol.ProtocolLibrary;
import com.microuhc.uhc.commands.*;
import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.game.*;
import com.microuhc.uhc.game.Timer;
import com.microuhc.uhc.gui.GUIManager;
import com.microuhc.uhc.protocol.data.TPSProvider;
import com.microuhc.uhc.scatter.Scatterer;
import com.microuhc.uhc.scenario.ScenarioManager;
import com.microuhc.uhc.user.User;
import com.microuhc.uhc.util.BlockUtils;
import com.microuhc.uhc.util.PacketUtils;
import com.microuhc.uhc.world.WorldManager;
import com.microuhc.uhc.world.ores.OreRates;
import com.microuhc.uhc.world.pregen.PregenManager;
import lombok.Getter;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;

public class MicroUHC extends JavaPlugin {
    public static final String PREFIX = ChatColor.DARK_GREEN + "Micro" + ChatColor.GOLD + "UHC " + ChatColor.GRAY;
    public static final String ARROW = "§8» §7";

    @Getter
    private static MicroUHC instance;

    private File db_file;
    private YamlConfiguration db_config;
    private GameState gameState;
    private UUID host;
    private int slots;
    private ConcurrentMap<UUID, User> userMap = new ConcurrentHashMap<>();
    private BossBar announceBar;
    private BukkitRunnable announceBarCallBack = null;

    private ScenarioManager scenarioManager = new ScenarioManager(this);
    private Scatterer scatterer = new Scatterer(this);

    private MessageListener messageListener = new MessageListener(this);
    private LoginListener loginListener = new LoginListener(this);
    private OceanDisabler oceanDisabler = new OceanDisabler(this);
    private CommandHandler cmds;

    //managers
    private GUIManager gui;
    private WorldManager worlds;

    //ore rates
    private OreRates oreRates;

    //tps
    private static final NumberFormat TPS_FORMATTER = NumberFormat.getNumberInstance();
    static {
        TPS_FORMATTER.setMaximumFractionDigits(2);
        TPS_FORMATTER.setMinimumFractionDigits(2);
        TPS_FORMATTER.setMinimumIntegerDigits(2);
    }

    //pregen
    PregenManager pregen;

    private TPSProvider tps;

    //game
    Game game;

    //uhc timer
    Timer timer;


    /*private BasicCommand[] commands = {
            new HostCommand(this),
            new RoleCommand(this),
            new FinishGameCommand(this),
            new ScatterCommand(this),
    };*/

    public Collection<User> getUsers(){
        return userMap.values();
    }

    public void onLoad(){
    }

    public void onEnable(){
        instance = this;

        PluginManager manager = Bukkit.getPluginManager();

        //setup config manager
        ConfigManager.getInstance().setup();

        quickLog("Loading database configuration");

        if(!getDataFolder().exists()){
            getDataFolder().mkdir();
        }
        db_file = new File(getDataFolder(), "settings.yml");
        if(!db_file.exists()){
            try {
                db_file.createNewFile();
            } catch (Throwable e) {
                exceptionLog(e, "Creating new configuration");
            }
        }
        db_config = new YamlConfiguration();
        try {
            db_config.load(db_file);
        } catch (Throwable e) {
            exceptionLog(e, "Loading configuration");
        }

        gameState = GameState.fromId(db_config.getInt("state", GameState.RESET.getId()));
        quickLog("Recovering with the gamestate " + gameState.getName() + " [" + gameState.getId() + "]");

        if(gameState.isBefore(GameState.FINISHED) && gameState.isAfter(GameState.PREHOST)){
            host = UUID.fromString(db_config.getString("host"));
            quickLog("Using host: " + host);
        }
        else{
            host = null;
        }

        new BukkitRunnable(){
            public void run() {
                if(gameState.is(GameState.RESET)){
                    Bukkit.getWhitelistedPlayers().clear();
                    Bukkit.setWhitelist(true);
                    gameState = GameState.PREHOST;
                }
            }
        }.runTask(this);

        slots = db_config.getInt("slots", 100);
        quickLog("Using slots: " + slots);

        quickLog("Setting up game");

        //setup game and timer
        game = new Game(this, ConfigManager.getInstance());
        timer = new Timer(this, game);

        game.setTimer(timer);
        game.setup();

        quickLog("Loading announce bar");

        announceBar = Bukkit.createBossBar("", BarColor.BLUE, BarStyle.SOLID);
        announceBar.setVisible(false);

        quickLog("Loading listeners");

        getServer().getPluginManager().registerEvents(oceanDisabler, this);
        getServer().getPluginManager().registerEvents(messageListener, this);
        getServer().getPluginManager().registerEvents(loginListener, this);

        quickLog("Loading users");

        ConfigurationSection users = db_config.getConfigurationSection("users");
        if(users != null) {
            for (String index : users.getKeys(false)) {
                UUID uuid = UUID.fromString(index);
                ConfigurationSection usersection = users.getConfigurationSection(index);
                User user = new User(uuid);
                user.setName(usersection.getString("name"));
                user.setRole(GameRole.getRole(usersection.getInt("role", 0)));
                userMap.put(uuid, user);
            }
        }


        else users = db_config.createSection("users");

        quickLog("Loaded " + users.getKeys(false).size() + " users");

        quickLog("Processing " + Bukkit.getOnlinePlayers().size() + " online players");

        for(Player p: Bukkit.getOnlinePlayers()){
            loginListener.procesJoin(p);
        }

        messageListener.registerProtocolListeners();

        oreRates = new OreRates(ConfigManager.getInstance());

        quickLog("Loading worlds");
        worlds = new WorldManager(ConfigManager.getInstance());
        worlds.loadWorlds();

        quickLog("Registering GUI menus");
        gui = new GUIManager(this);
        gui.registerGUIs(worlds);

        BlockUtils.setPlugin(this);

        //register commands
        quickLog("Registering commands");

        /*for(BasicCommand basicCommand: commands){
            basicCommand.setPermission("uhc.command." + basicCommand.getPermission());
            basicCommand.setPermissionMessage(PREFIX + ChatColor.RED + "You do not have permission for this command");
            try {
                basicCommand.register();
                quickLog("Registered /" + basicCommand.getName());
            } catch (Throwable e) {
                exceptionLog(e, "Registering command: " + basicCommand.getName());
            }
        }*/
        //register new command handler
        cmds = new CommandHandler(this);

        //register events
        manager.registerEvents(oreRates, this);

        //protocol stuff
        tps = new TPSProvider(this);
        tps.setup();

        //pregen
        pregen = new PregenManager(this);

        try {
            pregen.restore();
        } catch (CommandException e1) {
            Bukkit.getLogger().warning("Could not restore pregen task.");
        }

        //register commands
        cmds.registerCommands(game, gui, worlds, pregen);
    }

    public int getSlots(){
        return slots;
    }

    public void broadcast(BarColor barColor, String string, Object ... replace){
        announceBar.setTitle(String.format(string, replace));
        if(announceBarCallBack != null){
            announceBarCallBack.cancel();
        }
        announceBar.setProgress(0);
        announceBar.setVisible(true);
        announceBar.setColor(barColor);

        announceBarCallBack = new BukkitRunnable() {
            final double num = 0.025;
            double sofar = 0;
            public void run() {
                if((sofar += num) > 1){
                    cancel();
                    announceBar.setVisible(false);
                    announceBarCallBack = null;
                }
                else announceBar.setProgress(sofar);
            }
        };
        announceBarCallBack.runTaskTimerAsynchronously(this, 0, 1);
    }

    public void broadcast(Sound sound){
        for(Player player: Bukkit.getOnlinePlayers()){
            player.playSound(player.getLocation(), sound, 1f, 1f);
        }
    }

    public String getHostName(){
        return getHost() == null ? "None" : getHost().getName();
    }

    public void onDisable(){
        try {
            BasicCommand.unregisterAll();
        } catch (Throwable e) {
            exceptionLog(e, "Unregistering commands");
        }
        ProtocolLibrary.getProtocolManager().removePacketListeners(this);
        db_config.set("host", host == null ? null : host.toString());
        db_config.set("slots", slots);
        for(User user: userMap.values()){
            String uuidstring = user.getUuid().toString();
            ConfigurationSection configurationSection = db_config.createSection("users." + uuidstring);
            configurationSection.set("name", user.getName());
            configurationSection.set("role", user.getRole().getId());
        }
        try {
            db_config.save(db_file);
        } catch (Throwable e) {
            exceptionLog(e, "Could not save db config");
        }
        announceBar.setVisible(false);
    }

    public ScenarioManager getScenarioManager() {
        return scenarioManager;
    }

    public void quickLog(String s){
        getLogger().log(Level.INFO, s);
    }

    public void exceptionLog(Throwable ex, String cause){
        getLogger().log(Level.SEVERE, cause + " ", ex);
    }

    public static String getPREFIX() {
        return PREFIX;
    }

    public BossBar getAnnounceBar() {
        return announceBar;
    }

    public BukkitRunnable getAnnounceBarCallBack() {
        return announceBarCallBack;
    }

    public ConcurrentMap<UUID, User> getUserMap() {
        return userMap;
    }

    public User getHost() {
        return host == null ? null : userMap.get(host);
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setAnnounceBarCallBack(BukkitRunnable announceBarCallBack) {
        this.announceBarCallBack = announceBarCallBack;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public void setHost(User host) {
        this.host = host.getUuid();
        for(User user: getUsers()){
            if(user.isOnline()) PacketUtils.getInstance().updaterHeader(user);
        }
    }

    public void setHostUUID(UUID host){
        this.host = host;
        for(User user: getUsers()){
            if(user.isOnline()) PacketUtils.getInstance().updaterHeader(user);
        }
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public YamlConfiguration getDb_config() {
        return db_config;
    }

    public File getDb_file() {
        return db_file;
    }

    public static String repeat(int count, String with) {
        return new String(new char[count]).replace("\0", with);
    }

    public LoginListener getLoginListener() {
        return loginListener;
    }

    public MessageListener getMessageListener() {
        return messageListener;
    }

    /*public BasicCommand[] getCommands() {
        return commands;
    }*/

    public Scatterer getScatterer(){
        return scatterer;
    }

    public Game getGame() {
        return game;
    }

    public Timer getTimer() {
        return timer;
    }

    /**
     * Gets the servers tps.
     *
     * @return The servers tps.
     */
    public double getTps() {
        return tps.getAverage();
    }

    public String getFormattedTps() {
        return TPS_FORMATTER.format(getTps());
    }
}
