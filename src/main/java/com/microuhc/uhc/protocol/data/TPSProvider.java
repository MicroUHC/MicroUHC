package com.microuhc.uhc.protocol.data;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class TPSProvider implements AverageProvider {

    private final ContinuousAverager averager = new ContinuousAverager(20.0d);
    private final Plugin plugin;

    public TPSProvider(Plugin plugin) {
        this.plugin = plugin;
    }

    private boolean firstRun = true;
    private long lastTickTime;

    public void setup() {
        Bukkit.getScheduler().runTaskTimer(plugin, this::tick, 1L, 1L);
    }

    @Override
    public double getAverage(int amountOfValues) {
        return 1_000_000_000 / averager.getAverage(amountOfValues);
    }

    private void tick() {
        long now = System.nanoTime();

        if (firstRun) {
            firstRun = false;
            lastTickTime = now;
            return;
        }

        averager.addValue(now - lastTickTime);
        lastTickTime = now;
    }
}
