package com.microuhc.uhc.protocol.data;

public interface AverageProvider {
    double getAverage(int amountOfValues);

    default double getAverage() {
        return getAverage(100);
    }
}
