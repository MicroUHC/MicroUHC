package com.microuhc.uhc.protocol.data;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.linked.TDoubleLinkedList;
import gnu.trove.map.TIntDoubleMap;
import gnu.trove.map.hash.TIntDoubleHashMap;

public class ContinuousAverager implements AverageProvider {

    private final int maximumDataValues;
    private final double defaultValue;

    /**
     * Linked list of doubles stored in reversed order to not have to
     * {@link TDoubleLinkedList#reverse() reverse} on every average computation.
     * {@link TDoubleLinkedList#insert(int, double) Insert} instead of {@link TDoubleLinkedList#add(double) add}.
     */
    private final TDoubleLinkedList data = new TDoubleLinkedList();

    /**
     * Cache of amount of values to calculated averages, should be cleared whenever data is updated
     * ({@link #addValue(double)})
     */
    private final TIntDoubleMap cache = new TIntDoubleHashMap();

    public ContinuousAverager(int maximumDataValues, double defaultValue) {
        this.maximumDataValues = maximumDataValues;
        this.defaultValue = defaultValue;
    }

    public ContinuousAverager(double defaultValue) {
        this(/* Default: Keep data for 5 minutes assuming one value per tick*/ 20*60*5, defaultValue);
    }

    @Override
    public double getAverage(int amountOfValues) {
        if (data.isEmpty()) {
            return defaultValue;
        }

        if (cache.containsKey(amountOfValues)) {
            return cache.get(amountOfValues);
        }

        int available = Math.min(amountOfValues, data.size());
        TDoubleList averageData = available == data.size() ? data : data.subList(0, available);
        double result = averageData.sum() / ((double) available);
        cache.put(available, result);
        return result;
    }

    public void addValue(double value) {
        cache.clear();


        if (data.isEmpty()) {
            // Add value directly if empty (.insert NPEs on empty list?)
            data.add(value);
        } else {
            // Insert data at head of list for efficient .subList average computation
            data.insert(0, value);
        }

        // Junk elements at tail
        if (data.size() > maximumDataValues) {
            data.removeAt(data.size() - 1);
        }
    }

    public int getMaximumDataValues() {
        return maximumDataValues;
    }

    public double getDefaultValue() {
        return defaultValue;
    }

}
