package com.microuhc.uhc.util;

import java.io.*;
import java.util.logging.Level;

import com.microuhc.uhc.config.ConfigManager;
import org.bukkit.Bukkit;

import com.microuhc.uhc.MicroUHC;

public class FileUtils {

    /**
     * Delete all the minecraft playerdata and stats files.
     */
    public static void deletePlayerDataAndStats(MicroUHC plugin) {
        final File playerData = new File(Bukkit.getWorlds().get(0).getWorldFolder(), "playerdata");
        final File stats = new File(Bukkit.getWorlds().get(0).getWorldFolder(), "stats");

        int totalDatafiles = 0;
        int totalStatsfiles = 0;

        if (playerData.exists()) {
            for (File dataFiles : playerData.listFiles()) {
                try {
                    dataFiles.delete();
                    totalDatafiles++;
                } catch (Exception e) {
                    plugin.getLogger().warning("Could not delete " + dataFiles.getName() + ".");
                }
            }
        }

        if (stats.exists()) {
            for (File statsFiles : stats.listFiles()) {
                try {
                    statsFiles.delete();
                    totalStatsfiles++;
                } catch (Exception e) {
                    plugin.getLogger().warning("Could not delete " + statsFiles.getName() + ".");
                }
            }
        }

        plugin.getLogger().info("Deleted " + totalDatafiles + " player data files.");
        plugin.getLogger().info("Deleted " + totalStatsfiles + " player stats files.");
    }

    /**
     * Deletes the given file and it's subfiles.
     *
     * @return True if sucess, false otherwise.
     * @author D4mnX
     */
    public static boolean deleteFile(final File path) {
        if (!path.exists()) {
            return false;
        }

        final File files[] = path.listFiles();

        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                deleteFile(files[i]);
            } else {
                files[i].delete();
            }
        }

        return path.delete();
    }

    /**
     *  Loads a file as a stream and saves it
     * @param file the file to load
     */
    public static void loadFile(String file)
    {
        File t = new File(MicroUHC.getInstance().getDataFolder(), file);
        MicroUHC.getInstance().getLogger().log(Level.INFO, "Writing new file: " + t.getAbsolutePath());

        try {
            t.createNewFile();
            FileWriter out = new FileWriter(t);
            if (ConfigManager.getInstance().isDebugEnabled()) {
                System.out.println(file);
            }
            //InputStream is = getClass().getResourceAsStream("/"+file);
            InputStream is = MicroUHC.getInstance().getResource(file);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                out.write(line + "\n");
                if (ConfigManager.getInstance().isDebugEnabled()) {
                    System.out.print(line);
                }
            }
            out.flush();
            is.close();
            isr.close();
            br.close();
            out.close();

            MicroUHC.getInstance().getLogger().log(Level.INFO, "Loaded Config: " + file + "successfully!");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Moves an outdated file
     * @param oldConfig the old file to back up
     * @return the rile to rename the old file to
     */
    public static boolean moveFile(File oldConfig) {
        MicroUHC.getInstance().getLogger().log(Level.INFO, "Moving outdated config file: " + oldConfig.getName());
        String name = oldConfig.getName();
        File configBackup = new File(MicroUHC.getInstance().getDataFolder(), getNextName(name, 0));
        return oldConfig.renameTo(configBackup);
    }

    /**
     * generates a name to use for creating the backup file
     * @param name the name of the old config file
     * @param n the number to start with
     * @return the name of the new file to backup to
     */
    private static String getNextName(String name, int n){
        File oldConfig = new File(MicroUHC.getInstance().getDataFolder(), name+".old"+n);
        if(!oldConfig.exists()){
            return oldConfig.getName();
        }
        else{
            return getNextName(name, n+1);
        }
    }

}