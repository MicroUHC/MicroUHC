package com.microuhc.uhc.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.game.GameState;
import com.microuhc.uhc.scenario.Scenario;
import com.microuhc.uhc.user.User;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

public class PacketUtils {

    @Getter
    private static PacketUtils instance = new PacketUtils();

    private MicroUHC uhc = MicroUHC.getInstance();

    private PacketUtils() {

    }

    public void updaterHeader(User user){
        String scenarios = MicroUHC.getInstance().getScenarioManager().getScenarioList().isEmpty() ? "Vanilla " : "";
        for(Scenario scenario: uhc.getScenarioManager().getScenarioList()){
            scenarios += " " + scenario.getName() + ",";
        }
        String header = MicroUHC.PREFIX + "\n"
                + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + MicroUHC.repeat(50, " ") + ChatColor.GRAY + "<";
        String footer = "\n" + ChatColor.BLUE + "Host: " + ChatColor.WHITE + uhc.getHostName() +
                "\n" + ChatColor.BLUE + "Scenarios: " + ChatColor.WHITE + scenarios.substring(0, scenarios.length() - 1) +
                "\n" + ChatColor.BLUE + "Players: " + ChatColor.WHITE;
        if(uhc.getGameState().isAfter(GameState.POSTHOST) && uhc.getGameState().isBefore(GameState.FINISHED)){
            int i = 0;
            for(User otheruser: uhc.getUsers()){
                if(otheruser.isOnline() && otheruser.needsScatter()){
                    i++;
                }
            }
            footer += i + "/" + uhc.getSlots();
        }
        else{
            footer += Bukkit.getOnlinePlayers().size() + "/" + uhc.getSlots();
        }
        footer += "\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + MicroUHC.repeat(50, " ") + ChatColor.GRAY + "<";
        user.setHeaderFooter(uhc, header, footer);
    }

    public void updaterHeader(User user, int online){
        String scenarios = uhc.getScenarioManager().getScenarioList().isEmpty() ? "Vanilla " : "";
        for(Scenario scenario: uhc.getScenarioManager().getScenarioList()){
            scenarios += " " + scenario.getName() + ",";
        }
        String header = MicroUHC.PREFIX + "\n"
                + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + MicroUHC.repeat(50, " ") + ChatColor.GRAY + "<";
        String footer = "\n" + ChatColor.BLUE + "Host: " + ChatColor.WHITE + uhc.getHostName() +
                "\n" + ChatColor.BLUE + "Scenarios: " + ChatColor.WHITE + scenarios.substring(0, scenarios.length() - 1) +
                "\n" + ChatColor.BLUE + "Players: " + ChatColor.WHITE + online + "/" + uhc.getSlots() +
                "\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + MicroUHC.repeat(50, " ") + ChatColor.GRAY + "<";
        user.setHeaderFooter(uhc, header, footer);
    }

    public void sendAction(Player player, String message) {
        final PacketContainer title = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.CHAT);
        title.getChatComponents().write(0, WrappedChatComponent.fromText(message));
        title.getBytes().write(0, (byte) 2);
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, title);
        } catch (Throwable e1) {
            uhc.exceptionLog(e1, "Could not send tab packet");
        }
    }
}
