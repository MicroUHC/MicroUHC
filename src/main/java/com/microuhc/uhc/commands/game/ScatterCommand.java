package com.microuhc.uhc.commands.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.game.Game;
import com.microuhc.uhc.user.User;
import com.microuhc.uhc.util.PlayerUtils;
import net.md_5.bungee.api.*;
import org.bukkit.*;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;

public class ScatterCommand extends UHCCommand {

    private final Game game;

    public ScatterCommand(Game game) {
        super("scatter", "<teamscatter> <radius> <mindistance>");

        this.game = game;
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) throws CommandException {
        if(args.length < 3){
            return false;
        }

        boolean teamScatter = parseBoolean(args[0]);

        World world = game.getWorld();
        if (world == null) {
            throw new CommandException("Could not get world");
        }

        int radius;
        try {
            radius = Integer.parseInt(args[1]);
        } catch (Exception ex){
            throw new CommandException("Invalid radius number " + args[1]);
        }

        int mindistance;
        try {
            mindistance = Integer.parseInt(args[2]);
        } catch (Exception ex){
            throw new CommandException("Invalid mindistance number " + args[2]);
        }

        if (teamScatter) {
            throw new CommandException("Team scatter not implemented yet.");
        }

        plugin.getScatterer().findPlaces(world, radius, mindistance, new Runnable() {
            public void run() {
                plugin.getScatterer().scatterSolos();
            }
        });

        return true;
    }

    @Override
    public List<String> tabComplete(final CommandSender sender, final String[] args) {
        List<String> toReturn = new ArrayList<String>();

        if (args.length == 1) {
            toReturn.add("true");
            toReturn.add("false");
        }

        return toReturn;
    }

}
