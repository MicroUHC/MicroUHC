package com.microuhc.uhc.commands.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.util.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;

/**
 * Whitelist command class.
 * 
 * @author LeonTG77
 */
public class WhitelistCommand extends UHCCommand {

    public WhitelistCommand() {
        super("whitelist", "<on|off|add|remove|all|clear|list|prewl> [player]");
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) throws CommandException {
        if (args.length == 0) {
            return false;
        }

        if (args.length > 1) {
            if (args[0].equalsIgnoreCase("add")) {
                Player target = Bukkit.getPlayer(args[1]);

                if (target == null) {
                    OfflinePlayer offline = PlayerUtils.getOfflinePlayer(args[1]);

                    MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.GREEN + offline.getName() + " has been whitelisted.");
                    MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

                    offline.setWhitelisted(true);
                    return true;
                }

                MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.GREEN + target.getName() + " has been whitelisted.");
                MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

                target.setWhitelisted(true);
                return true;
            }

            if (args[0].equalsIgnoreCase("remove")) {
                Player target = Bukkit.getPlayer(args[1]);

                if (target == null) {
                    OfflinePlayer offline = PlayerUtils.getOfflinePlayer(args[1]);

                    MicroUHC.getInstance().broadcast(BarColor.RED, ChatColor.RED + offline.getName() + " is no longer whitelisted");
                    MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

                    offline.setWhitelisted(false);
                    return true;
                }

                MicroUHC.getInstance().broadcast(BarColor.RED, ChatColor.RED + target.getName() + " is no longer whitelisted");
                MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);


                target.setWhitelisted(false);
                return true;
            }
        }

        /*if (args[0].equalsIgnoreCase("prewl")) {
            if (game.preWhitelists()) {
                PlayerUtils.broadcast(Main.PREFIX + "Pre whitelists have been disabled.");
                game.setPreWhitelists(false);
                return true;
            }

            PlayerUtils.broadcast(Main.PREFIX + "Pre whitelists have been enabled.");
            game.setPreWhitelists(true);
            return true;
        }*/

        if (args[0].equalsIgnoreCase("on")) {
            if (Bukkit.hasWhitelist()) {
                throw new CommandException("The whitelist is already on.");
            }

            MicroUHC.getInstance().broadcast(BarColor.RED, ChatColor.RED + "The whitelist is now on");
            MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

            Bukkit.setWhitelist(true);

            /*if (game.getTeamSize().startsWith("No") || game.getTeamSize().startsWith("Open")) {
                game.setState(State.NOT_RUNNING);
            } else {
                game.setState(State.CLOSED);
            }*/
            return true;
        }

        if (args[0].equalsIgnoreCase("off")) {
            if (!Bukkit.hasWhitelist()) {
                throw new CommandException("The whitelist is not on.");
            }

            MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.GREEN + "The whitelist is now off");
            MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

            /*if (game.getTeamSize().startsWith("cTo")) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "timer 600 &7Whitelist is off for &8»&a");
            }

            if (game.getTeamSize().startsWith("FFA") || game.getTeamSize().startsWith("mTo") || game.getTeamSize().startsWith("RvB")) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "timer 300 &7Whitelist is off for &8»&a");
            }*/

            Bukkit.setWhitelist(false);
            return true;
        }

        if (args[0].equalsIgnoreCase("all")) {
            for (Player online : Bukkit.getOnlinePlayers()) {
                online.setWhitelisted(true);
            }

            MicroUHC.getInstance().broadcast(BarColor.GREEN, ChatColor.GREEN + "All players have been whitelisted");
            MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

            return true;
        }

        if (args[0].equalsIgnoreCase("clear")) {
            if (Bukkit.getWhitelistedPlayers().size() <= 0) {
                throw new CommandException("There are no whitelisted players.");
            }

            for (OfflinePlayer whitelisted : Bukkit.getWhitelistedPlayers()) {
                whitelisted.setWhitelisted(false);
            }

            MicroUHC.getInstance().broadcast(BarColor.RED, ChatColor.RED + "The whitelist has been cleared");
            MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);

            return true;
        }

        if (args[0].equalsIgnoreCase("list")) {
            if (Bukkit.getWhitelistedPlayers().size() <= 0) {
                throw new CommandException("There are no whitelisted players.");
            }

            StringBuilder list = new StringBuilder();
            int i = 1;

            for (OfflinePlayer whitelisted : Bukkit.getWhitelistedPlayers()) {
                if (list.length() > 0) {
                    if (i == Bukkit.getWhitelistedPlayers().size()) {
                        list.append(" and ");
                    } else {
                        list.append(", ");
                    }
                }

                list.append(whitelisted.getName());
                i++;
            }

            sender.sendMessage(MicroUHC.PREFIX + "There are §6" + (i - 1) + " §7whitelisted players");
            sender.sendMessage(MicroUHC.ARROW + "Wl'd players: §f" + list.toString() + ".");
            return true;
        }

        return false;
    }

    @Override
    public List<String> tabComplete(final CommandSender sender, final String[] args) {
        List<String> toReturn = new ArrayList<String>();

        if (args.length == 1) {
            toReturn.add("on");
            toReturn.add("off");
            toReturn.add("add");
            toReturn.add("remove");
            toReturn.add("all");
            toReturn.add("clear");
            toReturn.add("list");
            toReturn.add("prewl");
        }

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("add")) {
                for (Player online : Bukkit.getOnlinePlayers()) {
                    if (!online.isWhitelisted()) {
                        toReturn.add(online.getName());
                    }
                }
            }

            if (args[0].equalsIgnoreCase("remove")) {
                for (OfflinePlayer offline : Bukkit.getWhitelistedPlayers()) {
                    toReturn.add(offline.getName());
                }
            }
        }

        return toReturn;
    }
}