package com.microuhc.uhc.commands.game;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.game.Game;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class StartCommand extends UHCCommand {

    private final MicroUHC uhc;

    public StartCommand(MicroUHC uhc) {
        super("start", "");

        this.uhc = uhc;
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) throws CommandException {

        //start the game
        uhc.getTimer().start();

        return true;
    }

    @Override
    public List<String> tabComplete(final CommandSender sender, final String[] args) {
       return new ArrayList<>();
    }

}
