package com.microuhc.uhc.commands.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.config.MessageManager;
import com.microuhc.uhc.config.PrefixType;
import com.microuhc.uhc.game.GameRole;
import com.microuhc.uhc.game.MessageListener;
import com.microuhc.uhc.user.User;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.base.Joiner;
import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.game.Game;
import com.microuhc.uhc.game.GameState;
import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.gui.GUIManager;
//import com.microuhc.uhc.gui.guis.ConfigGUI;
//import com.microuhc.uhc.gui.guis.GameInfoGUI;
import com.microuhc.uhc.scenario.Scenario;
import com.microuhc.uhc.scenario.ScenarioManager;
import com.microuhc.uhc.util.NumberUtils;
import com.microuhc.uhc.util.PlayerUtils;

public class ConfigCommand extends UHCCommand {

    private final MicroUHC plugin;
    private final ConfigManager configManager;
    private final Game game;

    public ConfigCommand(MicroUHC plugin, ConfigManager configManager, Game game) {
        super("config", "<option> <value>");

        this.plugin = plugin;
        this.configManager = configManager;
        this.game = game;
    }

    public enum ConfigValue {
        APPLERATES, FLINTRATES, HEADSHEAL, HOST, MATCHPOST, MAXPLAYERS, MEETUP, PEARLDAMAGE, PVP, SCENARIOS, STALKING, SHEARRATES, STATE, TEAMSIZE, WORLD, BORDERSHRINKTIME;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            return false;
        }

        ConfigValue type;

        try {
            type = ConfigValue.valueOf(args[0].toUpperCase());
        } catch (Exception e) {
            StringBuilder types = new StringBuilder();
            int i = 1;

            for (ConfigValue value : ConfigValue.values()) {
                if (types.length() > 0) {
                    if (i == ConfigValue.values().length) {
                        types.append(" §7and§a ");
                    } else {
                        types.append("§7, §a");
                    }
                }

                types.append(value.name().toLowerCase());
                i++;
            }

            MessageManager.getInstance().sendPrefixMessage(PrefixType.Main, "Available config types: §a" + types.toString().trim() + "§7", sender);
            return true;
        }

        switch (type) {
            case HOST:
                if (args.length == 1) {
                    throw new CommandException("Usage: /config host <host/none>");
                }

                if(args[1].equalsIgnoreCase("none")){
                    if(game.getHost() != null){
                        game.getHost().setRole(GameRole.MOD);
                        if(game.getHost().isOnline()){
                            game.getHost().getPlayer().sendMessage(MicroUHC.PREFIX + ChatColor.GRAY + "You are no longer " + GameRole.HOST.getPrefix());

                            if (game.getGameState().equals(GameState.LOBBY)) {
                                //if they are still in the lobby they can rehost or remod themselves
                                game.getHost().getPlayer().setGameMode(GameMode.SURVIVAL);
                            }
                        }
                    }
                    game.setHostUUID(null);
                    plugin.broadcast(BarColor.BLUE, ChatColor.GRAY + "The " + GameRole.HOST.getPrefix() + " has been reset");
                } else {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target == null) {
                        if (sender instanceof Player) {
                            Player player = (Player) sender;
                            User user = plugin.getUserMap().get(player.getUniqueId());
                            user.sendBossBar(plugin, BarColor.RED, ChatColor.RED + "Player not found");
                            user.play(Sound.ENTITY_BLAZE_HURT);
                        } else {
                            sender.sendMessage(ChatColor.RED + "Player not found");
                        }
                    } else {
                        User targetuser = plugin.getUserMap().get(target.getUniqueId());
                        if (!targetuser.canChangeRole(GameRole.HOST)) {
                            if (sender instanceof Player) {
                                Player player = (Player) sender;
                                User user = plugin.getUserMap().get(player.getUniqueId());
                                user.sendBossBar(plugin, BarColor.RED, ChatColor.RED + "Player does not have permission to become a host");
                                user.play(Sound.ENTITY_BLAZE_HURT);
                            } else {
                                sender.sendMessage(ChatColor.RED + "Player does not have permission to become a host");
                            }
                        } else {
                            game.setHost(targetuser);
                            targetuser.setRole(GameRole.HOST);
                            targetuser.play(Sound.ENTITY_PLAYER_LEVELUP);
                            target.sendMessage(MicroUHC.PREFIX + "You are now " + GameRole.HOST.getPrefix());
                            targetuser.getPlayer().setGameMode(GameMode.SPECTATOR);
                            plugin.broadcast(BarColor.BLUE, ChatColor.translateAlternateColorCodes('&', targetuser.getPermissionUser().getPrefix()) + targetuser.getName() + ChatColor.GRAY + " is now " + GameRole.HOST.getPrefix());
                        }
                    }
                }
                break;
            case WORLD:
                if (args.length == 1) {
                    throw new CommandException("Usage: /config world <world>");
                }

                List<String> worlds = new ArrayList<String>();

                for (int i = 1; i < args.length; i++) {
                    World world = Bukkit.getWorld(args[i]);

                    if (world == null) {
                        throw new CommandException("'" + args[i] + "' is not an existing world.");
                    }

                    worlds.add(world.getName());
                }

                String worldList = Joiner.on(' ').join(Arrays.copyOfRange(args, 1, args.length));

                plugin.broadcast(BarColor.GREEN, "The game will now be played in '§a" + worldList.replaceAll(" ", "§7, §a") + "§7'.");
                plugin.broadcast(Sound.BLOCK_NOTE_BASS);

                game.setWorld(worlds.toArray(new String[worlds.size()]));
                break;
            default:
                return true;
        }

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        List<String> toReturn = new ArrayList<String>();

        if (args.length == 1) {
            for (ConfigValue type : ConfigValue.values()) {
                toReturn.add(type.name().toLowerCase());
            }
        }

        if (args.length == 2) {
            ConfigValue configType;

            try {
                configType = ConfigValue.valueOf(args[0].toUpperCase());
            } catch (Exception e) {
                return toReturn;
            }

            switch (configType) {
                case MATCHPOST:
                    toReturn.add("redd.it/######");
                    break;
                case STATE:
                    for (GameState type : GameState.values()) {
                        toReturn.add(type.getName().toLowerCase());
                    }
                    break;
                case TEAMSIZE:
                    toReturn.add("FFA");
                    toReturn.add("cTo");
                    toReturn.add("rTo");
                    toReturn.add("pTo");
                    toReturn.add("mTo");
                    toReturn.add("CapTo");
                    toReturn.add("Auction");
                    toReturn.add("No");
                    toReturn.add("Open");
                    break;
                case WORLD:
                    for (World world : Bukkit.getWorlds()) {
                        toReturn.add(world.getName());
                    }
                    break;
                default:
                    break;
            }

        }

        return toReturn;
    }

}
