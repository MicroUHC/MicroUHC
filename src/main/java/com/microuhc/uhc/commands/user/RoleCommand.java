package com.microuhc.uhc.commands.user;

import java.util.ArrayList;
import java.util.List;

import com.microuhc.uhc.gui.GUIManager;
import com.microuhc.uhc.gui.guis.SetRoleGUI;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.user.User;
import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.util.PlayerUtils;

public class RoleCommand extends UHCCommand {

    private final MicroUHC plugin;
    private final GUIManager gui;

    public RoleCommand(MicroUHC plugin, GUIManager gui) {
        super("role", "");

        this.plugin = plugin;
        this.gui = gui;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof Player)) {
            throw new CommandException("Only players can change their role.");
        }

        Player player = (Player) sender;
        User user = plugin.getUserMap().get(player.getUniqueId());
        SetRoleGUI setRoleGUI = gui.getGUI(SetRoleGUI.class);

        player.openInventory(setRoleGUI.get(user));

        return true;
    }

    @Override
    public List<String> tabComplete(final CommandSender sender, final String[] args) {
        return new ArrayList<>();
    }

}
