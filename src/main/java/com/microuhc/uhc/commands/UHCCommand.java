package com.microuhc.uhc.commands;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.util.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.microuhc.uhc.exceptions.CommandException;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.SimplePluginManager;

/**
 * Super class for commands.
 * 
 * @author LeonTG77
 */
public abstract class UHCCommand extends Parser {
    private String name, description, usage;
    private List<String> aliases;
    protected MicroUHC plugin = MicroUHC.getInstance();

    /**
     * Constructor for the uhc command super class.
     *
     * @param name The name of the command.
     * @param usage the command usage (after /command)
     */
    public UHCCommand(String name, String usage) {
        this.name = name;
        this.usage = usage;
    }

    /*public void register() throws NoSuchFieldException, IllegalAccessException{
        SimplePluginManager simplePluginManager = (SimplePluginManager) plugin.getServer().getPluginManager();
        SimpleCommandMap commandMap = Reflection.access(SimplePluginManager.class, simplePluginManager, "commandMap");
        register(commandMap);
        commandMap.register("uhc", this);
    }

    public static void unregisterAll() throws NoSuchFieldException, IllegalAccessException{
        SimplePluginManager simplePluginManager = (SimplePluginManager) Bukkit.getServer().getPluginManager();
        SimpleCommandMap commandMap = Reflection.access(SimplePluginManager.class, simplePluginManager, "commandMap");
        Map<String, Command> knownCommands = Reflection.access(SimpleCommandMap.class, commandMap, "knownCommands");
        for(Map.Entry<String, Command> commandEntry: new HashSet<>(knownCommands.entrySet())){
            if(commandEntry.getValue() instanceof BasicCommand){
                while(knownCommands.values().contains(commandEntry.getValue())){
                    knownCommands.values().remove(commandEntry.getValue());
                }
            }
        }
    }

    public void unregister() throws NoSuchFieldException, IllegalAccessException {
        SimplePluginManager simplePluginManager = (SimplePluginManager) IgniteUHC.getInstance().getServer().getPluginManager();
        SimpleCommandMap commandMap = Reflection.access(SimplePluginManager.class, simplePluginManager, "commandMap");
        Map<String, Command> knownCommands = Reflection.access(SimpleCommandMap.class, commandMap, "knownCommands");
        while(knownCommands.values().contains(this)){
            knownCommands.values().remove(this);
        }
        unregister(commandMap);
    }*/

    /**
     * Setup the 2 instances needed.
     * 
     * @param plugin The plugin instance.
     * @param game The game instance
     */
    protected void setupInstances(MicroUHC plugin) {
        this.plugin = plugin;
    }
    
    /**
     * Get the name of the command used after the /
     *
     * @return The command name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the usage of the command
     * <p>
     * Usage can be /nameofcommand [argurments...]
     *
     * @return The command usage.
     */
    public String getUsage() {
        return "/" + name + " " + usage;
    }

    /**
     * Return the permission of the command
     * <p>
     * The permission will be uhc.[nameofcommand]
     *
     * @return The command permission.
     */
    public String getPermission() {
        return "uhc." + name;
    }

    /**
     * Execute the command.
     *
     * @param sender The sender of the command.
     * @param args The argurments typed after the command.
     * @return True if successful, false otherwise. Returning false will send usage to the sender.
     *
     * @throws CommandException If anything was wrongly typed this is thrown sending the sender a warning.
     */
    public abstract boolean execute(CommandSender sender, String[] args) throws CommandException;

    /**
     * Tab complete the command.
     *
     * @param sender The sender of the command.
     * @param args The argurments typed after the command
     * @return A list of tab completable argurments.
     */
    public abstract List<String> tabComplete(CommandSender sender, String[] args);

    /**
     * Turn a the given boolean into "Enabled" or "Disabled".
     *
     * @param converting The boolean converting.
     * @return The converted boolean.
     */
    public String booleanToString(boolean converting) {
        return converting ? "enabled" : "disabled";
    }
}