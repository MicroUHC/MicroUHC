package com.microuhc.uhc.commands.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.game.Game;
import com.microuhc.uhc.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.gui.GUIManager;
import com.microuhc.uhc.gui.guis.WorldCreatorGUI;
import com.microuhc.uhc.util.PlayerUtils;
import com.microuhc.uhc.world.WorldManager;

/**
 * World command class.
 * 
 * @author LeonTG77
 */
public class WorldCommand extends UHCCommand {
    private final ConfigManager configManager;
    private final Game game;

    private final WorldManager manager;
    private final GUIManager gui;

    public WorldCommand(Game game, ConfigManager configManager, GUIManager gui, WorldManager manager) {
        super("world", "");

        this.configManager = configManager;
        this.game = game;

        this.manager = manager;
        this.gui = gui;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) throws CommandException {
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("create")) {
                if (!(sender instanceof Player)) {
                    throw new CommandException("Only players can create worlds");
                }

                Player player = (Player) sender;

                if (args.length < 3) {
                    throw new CommandException("Usage: /world create <name> <mapsize> [seed]");
                }

                WorldCreatorGUI creator = gui.getGUI(WorldCreatorGUI.class);

                if (!creator.get().getViewers().isEmpty()) {
                    throw new CommandException("Someone else is currently making a world.");
                }

                String worldname = args[1].toLowerCase();
                int diameter = parseInt(args[2], "diameter");

                long seed = new Random().nextLong();

                if (args.length > 3) {
                    try {
                        seed = parseLong(args[3]);
                    } catch (Exception e) {
                        seed = (long) args[3].hashCode();
                    }
                }

                player.openInventory(creator.get(worldname, diameter, seed));
                return true;
            }

            if (args[0].equalsIgnoreCase("delete")) {
                if (args.length == 1) {
                    throw new CommandException("Usage: /world delete <world>");
                }

                World world = Bukkit.getWorld(args[1]);

                if (world == null) {
                    throw new CommandException("The world '" + args[1] + "' does not exist.");
                }

                if (world.getName().equals("lobby")) {
                    throw new CommandException("You cannot delete the lobby world.");
                }

                if (game.getWorlds().contains(world)) {
                    throw new CommandException("You cannot delete a game world.");
                }

                if (!manager.deleteWorld(world)) {
                    throw new CommandException("Could not delete world '" + world.getName() + "'.");
                }

                plugin.broadcast(BarColor.RED, ChatColor.RED + "World '§6" + world.getName() + ChatColor.RED + " has been deleted.");
                plugin.broadcast(Sound.BLOCK_NOTE_BASS);
                //PlayerUtils.broadcastCmdInfo(sender, "Deleted world '" + world.getName() + "'.");
                return true;
            }

            if (args[0].equalsIgnoreCase("tp")) {
                if (!(sender instanceof Player)) {
                    throw new CommandException("Only players can teleport to worlds.");
                }

                Player player = (Player) sender;

                if (args.length == 1) {
                    throw new CommandException("Usage: /world tp <world>");
                }

                World world = Bukkit.getWorld(args[1]);

                if (world == null) {
                    throw new CommandException("The world '" + args[1] + "' does not exist.");
                }

                player.sendMessage(MicroUHC.PREFIX + "Teleported to world '§a" + world.getName() + "§7'.");
                player.teleport(world.getSpawnLocation());
                return true;
            }

            if (args[0].equalsIgnoreCase("list")) {
                sender.sendMessage(MicroUHC.PREFIX + "Default worlds: §8(§62§8)");
                sender.sendMessage(MicroUHC.ARROW + "lobby §8- §aNORMAL §8(§7Spawn World§8)");
                sender.sendMessage(MicroUHC.ARROW + "arena §8- §aNORMAL §8(§7Arena World§8)");
                sender.sendMessage(MicroUHC.PREFIX + "Game worlds: §8(§6" + (Bukkit.getWorlds().size() - 2) + "§8)");

                if ((Bukkit.getWorlds().size() - 2) == 0) {
                    sender.sendMessage(MicroUHC.ARROW + "There are no game worlds.");
                    return true;
                }

                for (World world : Bukkit.getWorlds()) {
                    switch (world.getName().toLowerCase()) {
                    case "lobby":
                    case "arena":
                        continue;
                    }

                    ChatColor color;

                    switch (world.getEnvironment()) {
                    case NETHER:
                        color = ChatColor.RED;
                        break;
                    case NORMAL:
                        color = ChatColor.GREEN;
                        break;
                    case THE_END:
                        color = ChatColor.AQUA;
                        break;
                    default:
                        return true;
                    }

                    sender.sendMessage(MicroUHC.ARROW + world.getName() + " §8- " + color + world.getEnvironment().name() + " §8(§7" + (game.getWorlds().contains(world) ? "§6In use" : "§7Not used")+ "§8)");
                }
                return true;
            }

            if (args[0].equalsIgnoreCase("load")) {
                if (args.length == 1) {
                    throw new CommandException("Usage: /world unload <world>");
                }

                World world = Bukkit.getWorld(args[1]);

                if (world != null) {
                    throw new CommandException("The world '" + args[1] + "' is already loaded.");
                }

                manager.loadWorld(args[1]);

                plugin.broadcast(BarColor.GREEN , ChatColor.GREEN + "World '§6" + args[1] + ChatColor.GREEN + " has been loaded.");
                plugin.broadcast(Sound.BLOCK_NOTE_BASS);

                //PlayerUtils.broadcastCmdInfo(sender, "Loaded world '" + args[1] + "'.");
                return true;
            }

            if (args[0].equalsIgnoreCase("unload")) {
                if (args.length == 1) {
                    throw new CommandException("Usage: /world unload <world>");
                }

                World world = Bukkit.getWorld(args[1]);

                if (world == null) {
                    throw new CommandException("The world '" + args[1] + "' does not exist.");
                }

                if (world == Bukkit.getWorlds().get(0)) {
                    throw new CommandException("You cannot unload the lobby world.");
                }

                if (game.getWorlds().contains(world)) {
                    throw new CommandException("You cannot unload a game world.");
                }

                manager.unloadWorld(world);

                plugin.broadcast(BarColor.RED, "§7World '§a" + world.getName() + "§7' has been unloaded.");
                plugin.broadcast(Sound.BLOCK_NOTE_BASS);

                //PlayerUtils.broadcastCmdInfo(sender, "Unloaded world '" + world.getName() + "'.");
                return true;
            }

            if (args[0].equalsIgnoreCase("info")) {
                World world = null;
                
                
                if (args.length == 1) {
                    if (!(sender instanceof Player)) {
                        throw new CommandException("Usage: /world info <world>");
                    }
                    
                    Player player = (Player) sender;
                    
                    world = player.getWorld();
                } else {
                    world = Bukkit.getWorld(args[1]);

                    if (world == null) {
                        throw new CommandException("The world '" + args[1] + "' does not exist.");
                    }
                }

                ConfigurationSection section = configManager.getWorlds().getConfigurationSection(world.getName());
                
                sender.sendMessage(MicroUHC.PREFIX + "Information about world '§a" + world.getName() + "§7'.");
                sender.sendMessage(MicroUHC.ARROW + "World Type: §a" + section.getString("worldtype").toLowerCase());
                sender.sendMessage(MicroUHC.ARROW + "Seed: §a" + section.getLong("seed"));
                sender.sendMessage(MicroUHC.ARROW + "Center: §a" + section.getInt("center.x") + " " + section.getInt("center.z"));
                sender.sendMessage(MicroUHC.ARROW + "Size: §a" + section.getInt("diameter"));
                sender.sendMessage(MicroUHC.ARROW + "1.8 Stone: §a" + section.getBoolean("newStone"));
                sender.sendMessage(MicroUHC.ARROW + "Anti Stripmine §a" + section.getBoolean("antiStripmine"));
                sender.sendMessage(MicroUHC.ARROW + "Ore Rates: §a" + section.getString("oreRates").toLowerCase());
                return true;
            }
        }

        sender.sendMessage(MicroUHC.PREFIX + "World management help:");
        sender.sendMessage("§8» §a/world create §8- §7§oCreate a world.");
        sender.sendMessage("§8» §a/world delete §8- §7§oDelete a world.");
        sender.sendMessage("§8» §a/world info §8- §7§oView world info.");
        sender.sendMessage("§8» §a/world load §8- §7§oLoad a world.");
        sender.sendMessage("§8» §a/world unload §8- §7§oUnload a world.");
        sender.sendMessage("§8» §a/world list §8- §7§oList all worlds.");
        sender.sendMessage("§8» §a/world tp §8- §7§oTeleport to a world.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        List<String> toReturn = new ArrayList<String>();

        if (args.length == 1) {
            toReturn.add("create");
            toReturn.add("delete");
            toReturn.add("info");
            toReturn.add("list");
            toReturn.add("tp");
            toReturn.add("load");
            toReturn.add("unload");
        }

        if (args.length == 2) {
            switch (args[0].toLowerCase()) {
            case "create":
                /*for (String path : settings.getHOF().getKeys(false)) {
                    toReturn.add(path.toLowerCase());
                }
                break;*/
            case "load":
            case "unload":
            case "tp":
            case "delete":
            case "info":
                for (World world : Bukkit.getWorlds()) {
                    toReturn.add(world.getName());
                }
                break;
            }
        }

        if (args.length == 3) {
            if (args[0].equalsIgnoreCase("create")) {
                toReturn.add("2000");
                toReturn.add("3000");
            }
        }

        if (args.length == 4) {
            if (args[0].equalsIgnoreCase("create")) {
                toReturn.add("" + new Random().nextLong());
            }
        }

        return toReturn;
    }
}