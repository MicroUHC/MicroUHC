package com.microuhc.uhc.commands.world;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.util.PlayerUtils;
import com.microuhc.uhc.world.pregen.PregenManager;

/**
 * Pregen command class.
 * 
 * @author LeonTG77
 */
public class PregenCommand extends UHCCommand {
    private final PregenManager pregen;
    
    public PregenCommand(PregenManager pregen) {
        super("pregen", "<world|cancel|pause> <diameter> [speed] [force]");
        
        this.pregen = pregen;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) throws CommandException {
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("cancel")) {
                pregen.stopPregen();
                PlayerUtils.broadcast(PregenManager.PREFIX + "The pregen has been cancelled.");
                return true;
            }

            if (args[0].equalsIgnoreCase("pause")) {
                if (pregen.pausePregen()) {
                    PlayerUtils.broadcast(PregenManager.PREFIX + "The pregen has been paused.");
                } else {
                    PlayerUtils.broadcast(PregenManager.PREFIX + "The pregen has been un-paused.");
                }
                return true;
            }
        }

        if (args.length < 2) {
            return false;
        }

        World world = Bukkit.getWorld(args[0]);

        if (world == null) {
            throw new CommandException("The world '" + args[0] + "' does not exist.");
        }
        
        int diameter = parseInt(args[1], "diameter");

        boolean forceLoad = false;
        int speed = 80;
        
        if (args.length > 2) {
            speed = parseInt(args[2], "speed");
        }
        
        if (args.length > 3) {
            forceLoad = parseBoolean(args[3]);
        }

        pregen.startPregen(world, diameter / 2, speed, forceLoad);
        PlayerUtils.broadcast(PregenManager.PREFIX + "Starting pregen of world '§a" + world.getName() + "§7' with size of §6" + diameter + "x" + diameter + "§7.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        List<String> toReturn = new ArrayList<String>();

        if (args.length == 1) {
            for (World world : Bukkit.getWorlds()) {
                toReturn.add(world.getName());
            }

            toReturn.add("pause");
            toReturn.add("cancel");
        }

        if (args.length == 2) {
            World world = Bukkit.getWorld(args[0]);

            if (world != null) {
                toReturn.add("" + ((int) world.getWorldBorder().getSize()));
            }
        }

        if (args.length == 3) {
            toReturn.add("60");
            toReturn.add("80");
            toReturn.add("420");
            toReturn.add("666");
        }

        if (args.length == 4) {
            toReturn.add("true");
            toReturn.add("false");
        }

        return toReturn;
    }
}