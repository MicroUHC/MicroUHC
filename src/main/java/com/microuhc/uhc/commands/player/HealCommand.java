package com.microuhc.uhc.commands.player;

import java.util.ArrayList;
import java.util.List;

import com.microuhc.uhc.MicroUHC;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.microuhc.uhc.user.User;
import com.microuhc.uhc.commands.UHCCommand;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.util.PlayerUtils;

/**
 * Heal command class.
 * 
 * @author LeonTG77
 */
public class HealCommand extends UHCCommand {
    private final MicroUHC plugin;

    public HealCommand(MicroUHC plugin) {
        super("heal", "[player]");

        this.plugin = plugin;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                throw new CommandException("Only players can heal themselves.");
            }

            Player player = (Player) sender;
            User user = plugin.getUserMap().get(player.getUniqueId());

            player.sendMessage(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " You have been healed.");
            user.resetHealth();
            return true;
        }

        if (args[0].equals("*")) {
            for (Player online : Bukkit.getOnlinePlayers()) {
                User user = plugin.getUserMap().get(online.getUniqueId());
                user.resetHealth();
            }

            PlayerUtils.broadcast(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " All players have been healed.");
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);

        if (target == null) {
            throw new CommandException("'" + args[0] + "' is not online.");
        }

        User user = plugin.getUserMap().get(target.getUniqueId());
        user.resetHealth();

        sender.sendMessage(MicroUHC.PREFIX + " " + MicroUHC.ARROW + " You healed §a" + target.getName() + "§7.");
        target.sendMessage(MicroUHC.PREFIX  + " " + MicroUHC.ARROW + " You have been healed.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            return allPlayers();
        }

        return new ArrayList<String>();
    }
}