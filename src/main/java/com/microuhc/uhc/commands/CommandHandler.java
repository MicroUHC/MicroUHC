package com.microuhc.uhc.commands;

import java.util.ArrayList;
import java.util.List;


import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.commands.game.ConfigCommand;
import com.microuhc.uhc.commands.game.StartCommand;
import com.microuhc.uhc.commands.game.WhitelistCommand;
import com.microuhc.uhc.commands.player.FeedCommand;
import com.microuhc.uhc.commands.player.HealCommand;
import com.microuhc.uhc.commands.user.*;
import com.microuhc.uhc.commands.user.RoleCommand;
import com.microuhc.uhc.commands.world.PregenCommand;
import com.microuhc.uhc.commands.world.WorldCommand;
import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.game.Game;
import com.microuhc.uhc.gui.GUIManager;
import com.microuhc.uhc.user.User;
import com.microuhc.uhc.world.WorldManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.scenario.ScenarioManager;
import com.microuhc.uhc.commands.game.ScatterCommand;
import org.bukkit.entity.Player;
import com.microuhc.uhc.world.pregen.PregenManager;

/**
 * Command handler class.
 * 
 * @author LeonTG77
 */
public class CommandHandler implements CommandExecutor, TabCompleter {
    private final MicroUHC plugin;

    public CommandHandler(MicroUHC plugin) {
        this.plugin = plugin;
    }

    private List<UHCCommand> cmds = new ArrayList<UHCCommand>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        UHCCommand command = getCommand(cmd.getName());

        if (command == null) { // this shouldn't happen, it only uses registered commands but incase.
            return true;
        }

        if (!sender.hasPermission(command.getPermission())) {
            sender.sendMessage(ChatColor.RED + "You don't have permission to execute this command.");
            return true;
        }

        try {
            if (!command.execute(sender, args)) {

                if(sender instanceof Player){
                    Player cmdSender = (Player) sender;
                    User user = plugin.getUserMap().get(cmdSender.getUniqueId());
                    user.sendBossBar(plugin, BarColor.RED, ChatColor.RED + "Invalid usage: " + command.getUsage());
                    user.play(Sound.ENTITY_BLAZE_HURT);
                } else {
                    sender.sendMessage(MicroUHC.PREFIX + ChatColor.RED + "Invalid usage: " + command.getUsage());
                }

            }
        } catch (CommandException ex) {

            // send them the exception message
            if(sender instanceof Player){
                Player cmdSender = (Player) sender;
                User user = plugin.getUserMap().get(cmdSender.getUniqueId());
                user.sendBossBar(plugin, BarColor.RED, ChatColor.RED + ex.getMessage());
                user.play(Sound.ENTITY_BLAZE_HURT);
            } else {
                sender.sendMessage(MicroUHC.PREFIX + ChatColor.RED + ex.getMessage());
            }

        } catch (Exception ex) {

            // send them the exception and tell the console the error if its not a command exception
            if(sender instanceof Player){
                Player cmdSender = (Player) sender;
                User user = plugin.getUserMap().get(cmdSender.getUniqueId());
                user.sendBossBar(plugin, BarColor.RED, ChatColor.RED + ex.getClass().getName() + ": " + ex.getMessage());
                user.play(Sound.ENTITY_BLAZE_HURT);
            } else {
                sender.sendMessage(MicroUHC.PREFIX + ChatColor.RED + ex.getClass().getName() + ": " + ex.getMessage());
            }

            ex.printStackTrace();
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        UHCCommand command = getCommand(cmd.getName());

        if (command == null) { // this shouldn't happen, it only uses registered commands but incase.
            return null;
        }

        if (!sender.hasPermission(command.getPermission())) {
            return null;
        }

        try {
            List<String> list = command.tabComplete(sender, args);

            // if the list is null, replace it with everyone online.
            if (list == null) {
                return null;
            }

            // I don't want anything done if the list is empty.
            if (list.isEmpty()) {
                return list;
            }

            List<String> toReturn = new ArrayList<String>();

            if (args[args.length - 1].isEmpty()) {
                for (String type : list) {
                    toReturn.add(type);
                }
            } else {
                for (String type : list) {
                    if (type.toLowerCase().startsWith(args[args.length - 1].toLowerCase())) {
                        toReturn.add(type);
                    }
                }
            }

            return toReturn;
        } catch (Exception ex) {
            sender.sendMessage(ChatColor.RED + ex.getClass().getName() + ": " + ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Get a uhc command.
     *
     * @param name The name of the uhc command
     * @return The UHCCommand if found, null otherwise.
     */
    protected UHCCommand getCommand(String name) {
        for (UHCCommand cmd : cmds) {
            if (cmd.getName().equalsIgnoreCase(name)) {
                return cmd;
            }
        }
        
        return null;
    }

    /**
     * Register all the commands.
     */
    public void registerCommands(Game game, GUIManager gui, WorldManager world, PregenManager pregen) {
        // arena


        // basic


        // game
        cmds.add(new ConfigCommand(plugin, ConfigManager.getInstance(), game));
        cmds.add(new ScatterCommand(game));
        cmds.add(new WhitelistCommand());
        cmds.add(new StartCommand(plugin));

        // give


        // inventory


        // lag


        // msg


        // player
        cmds.add(new FeedCommand(plugin));
        cmds.add(new HealCommand(plugin));

        // punish


        // spectate


        // team

        // user
        cmds.add(new RoleCommand(plugin, gui));

        // world
        cmds.add(new WorldCommand(game, ConfigManager.getInstance(), gui, world));
        cmds.add(new PregenCommand(pregen));

        for (UHCCommand cmd : cmds) {

            PluginCommand pCmd = plugin.getCommand(cmd.getName());

            cmd.setupInstances(plugin);

            // if its null, broadcast the command name so I know which one it is (so I can fix it).
            if (pCmd == null) {
                MicroUHC.getInstance().broadcast(BarColor.RED, ChatColor.RED + cmd.getName());
                MicroUHC.getInstance().broadcast(Sound.BLOCK_NOTE_BASS);
                continue;
            }

            pCmd.setExecutor(this);
            pCmd.setTabCompleter(this);
        }
    }
}