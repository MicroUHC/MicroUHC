package com.microuhc.uhc.world.pregen;

import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.events.other.PregenFinishEvent;
import com.microuhc.uhc.exceptions.CommandException;

/**
 * Pregen manager class.
 * 
 * @author LeonTG77 with some stuff from WorldBorder.
 */
public class PregenManager implements Listener {
    public static final String PREFIX = "§9Pregen §8» §7";

    private final MicroUHC plugin;

    public PregenManager(MicroUHC plugin) {
        this.plugin = plugin;
        
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void on(PregenFinishEvent event) {
        task = null;
    }
    
    private PregenTask task = null;

    /**
     * Restore the pregen task if there's any.
     * 
     * @throws CommandException If the world doesn't exist or it's world data can't be loaded.
     */
    public void restore() throws CommandException {
        ConfigurationSection section = ConfigManager.getData().getConfigurationSection("pregen task");
        
        if (section != null) {
            String worldName = section.getString("world");
            
            int radius = section.getInt("radius", 208);
            int chunksPerRun = section.getInt("chunksPerRun", 5);
            int tickFrequency = section.getInt("tickFrequency", 20);
            
            int fillX = section.getInt("x", 0);
            int fillZ = section.getInt("z", 0);
            int fillLength = section.getInt("length", 0);
            int fillTotal = section.getInt("total", 0);
            
            boolean forceLoad = section.getBoolean("forceLoad", false);
            
            task = new PregenTask(plugin, worldName, radius, chunksPerRun, tickFrequency, forceLoad);
            task.runTaskTimer(plugin, tickFrequency, tickFrequency);

            //set state to genning
            plugin.setGameState(GameState.GENNING);

            task.continueProgress(fillX, fillZ, fillLength, fillTotal);

            ConfigManager.getData().set("pregen task", null);
            ConfigManager.getInstance().saveData();
        }
    }
    
    /**
     * Start a new pregen with the given settings.
     * 
     * @param world The world to pregen.
     * @param radius The radius to pregen with.
     * @param speed The pregen speed.
     * @param forceLoad True to pregen already existing chunks.
     * 
     * @throws CommandException If a pregen is already running.
     */
    public void startPregen(World world, int radius, int speed, boolean forceLoad) throws CommandException {
        if (isPregenning()) {
            throw new CommandException("A pregen is already running.");
        }
        
        int ticks = 1, repeats = 1;
        
        if (speed > 20) { // taken from world border's code.
            repeats = speed / 20;
        } else {
            ticks = 20 / speed;
        }

        plugin.setGameState(GameState.GENNING);

        task = new PregenTask(plugin, world.getName(), radius, repeats, ticks, forceLoad);
        task.runTaskTimer(plugin, ticks, ticks);
    }
    
    /**
     * Stop the current pregen that is running
     * 
     * @throws CommandException If no pregens are running.
     */
    public void stopPregen() throws CommandException {
        if (!isPregenning()) {
            throw new CommandException("A pregen is not running.");
        }
        
        task.cancel();
        task = null;

        plugin.setGameState(GameState.LOBBY);
    }
    
    /**
     * Pause the current pregen.
     * 
     * @return The new paused state.
     * @throws CommandException If no pregens are running.
     */
    public boolean pausePregen() throws CommandException {
        if (!isPregenning()) {
            throw new CommandException("A pregen is not running.");
        }
     
        task.pause();
        return task.isPaused();
    }
    
    /**
     * Check if a pregen is currently running.
     * 
     * @return True if it is, false otherwise.
     */
    public boolean isPregenning() {
        return task != null;
    }
}