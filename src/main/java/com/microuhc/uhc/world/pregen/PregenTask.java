package com.microuhc.uhc.world.pregen;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.config.ConfigManager;
import com.microuhc.uhc.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.scheduler.BukkitRunnable;

import com.microuhc.uhc.events.other.PregenFinishEvent;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.util.NumberUtils;
import com.microuhc.uhc.util.PacketUtils;
import com.microuhc.uhc.util.PlayerUtils;
import com.microuhc.uhc.world.pregen.data.CoordXZ;
import com.microuhc.uhc.world.pregen.data.WorldData;

/**
 * Pregen task class.
 * 
 * @author Original: WorldBorder, Fixed/Cleaned up by LeonTG77
 */
public class PregenTask extends BukkitRunnable {
    private static final String PREGEN_MSG = "§7Pregenning '§a%s§7' §8(§a%s §8/ §7TPS: §a%s §8/ §7RAM: §a%s§7/§a%s§8)";
    
    private static final int AUTOSAVE_FREQUENCY = 30;
    private static final int EXTRA_DISTANCE = 208;
    
    private static final int MEMORY_TOLLERANCE = 500;
    private static final int MEMORY_TOO_LOW = 200;
    
    private static final int TPS_TOLLERANCE = 17;
    private static final int TPS_TOO_LOW = 11;
    
    private final Runtime runtime = Runtime.getRuntime();

    private final MicroUHC plugin;
    
    private final WorldData worldData;
    private final String worldName;

    private boolean pausedForMemory = false;
    private boolean pausedForTPS = false;
    
    private boolean readyToGo = false;
    private boolean paused = false;
    
    private int chunksPerRun = 1;
    
    private boolean forceLoad = false;

    private final int radius;
    
    private final int minX;
    private final int maxX;
    
    private final int minZ;
    private final int maxZ;

    private CoordXZ lastChunk = new CoordXZ(0, 0);
    private int chunkX = 0;
    private int chunkZ = 0;
    
    private int tickFrequency = 1;
    private int refX = 0, lastLegX = 0;
    private int refZ = 0, lastLegZ = 0;
    private int refLength = -1;
    private int refTotal = 0, lastLegTotal = 0;

    private boolean isZLeg = false;
    private boolean isNeg = false;
    
    private int length = -1;
    private int current = 0;
    
    private boolean insideBorder = true;
    
    private List<CoordXZ> storedChunks = new LinkedList<CoordXZ>();
    private Set<CoordXZ> originalChunks = new HashSet<CoordXZ>();

    private long lastAutosave = System.currentTimeMillis();
    private long lastReport = System.currentTimeMillis();

    private int reportTarget = 0;
    private int reportTotal = 0;
    private int reportNum = 0;

    /**
     * Pregen task class constructor.
     * 
     * @param plugin The plugin instance.
     *
     * @param worldName The world's name to pregen in.
     * @param radius The radius to use for the pregen.
     * @param chunksPerRun How many chunks to pregen per run.
     * @param tickFrequency How often in ticks the task will run.
     * @param forceLoad Wether to force pregen already pregenned chunks.
     * 
     * @throws CommandException If world does not exist or it's world data fails to load.
     */
    public PregenTask(MicroUHC plugin, String worldName, int radius, int chunksPerRun, int tickFrequency, boolean forceLoad) throws CommandException {
        this.worldName = worldName;
        
        World world = Bukkit.getWorld(worldName);
        
        if (world == null) {
            throw new CommandException("The world '" + worldName + "' does not exist.");
        }
        
        this.plugin = plugin;
        
        this.radius = radius + EXTRA_DISTANCE;
        
        this.tickFrequency = tickFrequency;
        this.chunksPerRun = chunksPerRun;
        this.forceLoad = forceLoad;
        
        WorldBorder border = world.getWorldBorder();
        
        this.minX = border.getCenter().getBlockX() - radius;
        this.maxX = border.getCenter().getBlockX() + radius;

        this.minZ = border.getCenter().getBlockZ() - radius;
        this.maxZ = border.getCenter().getBlockZ() + radius;

        this.chunkX = CoordXZ.blockToChunk(border.getCenter().getBlockX());
        this.chunkZ = CoordXZ.blockToChunk(border.getCenter().getBlockZ());

        this.worldData = new WorldData(world);

        int chunkWidthX = (int) Math.ceil((double) ((radius + 16) * 2) / 16);
        int chunkWidthZ = (int) Math.ceil((double) ((radius + 16) * 2) / 16);
        
        int biggerWidth = (chunkWidthX > chunkWidthZ) ? chunkWidthX : chunkWidthZ;
        
        this.reportTarget = (biggerWidth * biggerWidth) + biggerWidth + 1;

        Chunk[] originals = world.getLoadedChunks();
        
        for (Chunk original : originals) {
            originalChunks.add(new CoordXZ(original.getX(), original.getZ()));
        }

        this.readyToGo = true;
    }

    /**
     * Continue the pregen task progress from the given settings.
     * 
     * @param x The current chunk X coord.
     * @param z The current chunk Z coord.
     * @param length The current length.
     * @param totalDone The current total done chunks.
     */
    public void continueProgress(int x, int z, int length, int totalDone) {
        this.reportTotal = totalDone;
        this.length = length;
        
        this.chunkX = x;
        this.chunkZ = z;
    }
    
    /**
     * Get the message used for the console/action bar with the given world's name.
     * 
     * @param world The world to use.
     * @return The message.
     */
    private String getMessage(World world) {
        return getMessage(world, (((double) (reportTotal + reportNum) / (double) reportTarget) * 100));
    }
    
    /**
     * Get the message used for the console/action bar with the given world's name with the given completed percent.
     * 
     * @param world The world to use.
     * @param perc The percent to display.
     * 
     * @return The message.
     */
    private String getMessage(World world, double perc) {
        String TPS = plugin.getFormattedTps();

        long maxRAM = 4096;
        long current = maxRAM - getAvailableMemory();
        
        return String.format(PREGEN_MSG, world.getName(), NumberUtils.formatPercentDouble(perc) + "%", TPS, current, maxRAM);
    }
    
    @Override
    public void run() {
        int memory = getAvailableMemory();

        if (pausedForMemory) {
            if (memory < MEMORY_TOLLERANCE) {
                return;
            }

            pausedForMemory = false;
            readyToGo = true;
            
            PlayerUtils.broadcast(PregenManager.PREFIX + "Memory is now good enough, unpausing pregen.");
        }
        
        if (pausedForTPS) {
            if (plugin.getTps() <= TPS_TOLLERANCE) {
                return;
            }

            pausedForTPS = false;
            readyToGo = true;
            
            PlayerUtils.broadcast(PregenManager.PREFIX + "TPS is now good enough, unpausing pregen.");
        }

        if (!readyToGo || paused) {
            return;
        }

        readyToGo = false;
        
        long loopStartTime = System.currentTimeMillis();
        
        World world = Bukkit.getWorld(worldName);
        
        if (world == null) {
            PlayerUtils.broadcast(PregenManager.PREFIX + "World no longer available, cancelling pregen.");
            cancel();
            return;
        }

        for (int loop = 0; loop < chunksPerRun; loop++) {
            if (isPaused()) { // in case the task has been paused while we're repeating...
                return;
            }

            long now = System.currentTimeMillis();
            
            if (now > lastReport + 5000) {
                lastReport = System.currentTimeMillis();
                
                Bukkit.getLogger().info(PregenManager.PREFIX + getMessage(world));
                
                reportTotal += reportNum;
                reportNum = 0;
                
                if (lastAutosave + (AUTOSAVE_FREQUENCY * 1000) < lastReport) {
                    lastAutosave = lastReport;
                    
                    PlayerUtils.broadcast(PregenManager.PREFIX + "Saving world to be safe...");
                    world.save();
                    PlayerUtils.broadcast(PregenManager.PREFIX + "Saved world.");
                }
                
                if (memory < MEMORY_TOO_LOW) {
                    pausedForMemory = true;
                    storeSettings();
                
                    PlayerUtils.broadcast(PregenManager.PREFIX + "Memory is too low, temporarily pausing task.");
                    
                    System.gc();
                }
                
                if (plugin.getTps() <= TPS_TOO_LOW) {
                    pausedForTPS = true;
                    storeSettings();
                    
                    PlayerUtils.broadcast(PregenManager.PREFIX + "TPS is too low, temporarily pausing task.");
                }
            }
            
            Bukkit.getOnlinePlayers().forEach(online -> PacketUtils.getInstance().sendAction(online, getMessage(world)));
            
            if (now > loopStartTime + 45) {
                readyToGo = true;
                return;
            }
            
            while (!isInsideBorder(CoordXZ.chunkToBlock(chunkX) + 8, CoordXZ.chunkToBlock(chunkZ) + 8)) {
                if (!moveToNext(world)) {
                    return;
                }
            }
            
            insideBorder = true;

            if (!forceLoad) {
                while (worldData.isChunkFullyGenerated(chunkX, chunkZ)) {
                    insideBorder = true;
                    
                    if (!moveToNext(world)) {
                        return;
                    }
                }
            }

            world.loadChunk(chunkX, chunkZ, true);
            worldData.chunkExistsNow(chunkX, chunkZ);

            // Population requires more loaded chunks
            int popX = !isZLeg ? chunkX : (chunkX + (isNeg ? -1 : 1));
            int popZ = isZLeg ? chunkZ : (chunkZ + (!isNeg ? -1 : 1));
            
            world.loadChunk(popX, popZ, false);

            if (!storedChunks.contains(lastChunk) && !originalChunks.contains(lastChunk)) {
                world.loadChunk(lastChunk.getX(), lastChunk.getZ(), false);
                storedChunks.add(new CoordXZ(lastChunk.getX(), lastChunk.getZ()));
            }

            storedChunks.add(new CoordXZ(popX, popZ));
            storedChunks.add(new CoordXZ(chunkX, chunkZ));

            while (storedChunks.size() > 8) {
                CoordXZ coord = storedChunks.remove(0);
                
                if (!originalChunks.contains(coord)) {
                    world.unloadChunkRequest(coord.getX(), coord.getZ());
                }
            }

            if (!moveToNext(world)) {
                return;
            }
        }

        readyToGo = true;
    }

    /**
     * Move to a new chunk to pregen.
     * 
     * @param world The world to move in.
     * @return It's success.
     */
    private boolean moveToNext(World world) {
        if (isPaused()) {
            return false;
        }

        reportNum++;

        if (!isNeg && current == 0 && length > 3) {
            if (!isZLeg) {
                lastLegX = chunkX;
                lastLegZ = chunkZ;
                lastLegTotal = reportTotal + reportNum;
            } else {
                refX = lastLegX;
                refZ = lastLegZ;
                refTotal = lastLegTotal;
                refLength = length - 1;
            }
        }
        
        if (current < length) {
            current++;
        } else {
            current = 0;
            isZLeg ^= true;
            
            if (isZLeg) {
                isNeg ^= true;
                length++;
            }
        }

        lastChunk.setX(chunkX);
        lastChunk.setZ(chunkZ);

        if (isZLeg) {
            chunkZ += (isNeg) ? -1 : 1;
        } else {
            chunkX += (isNeg) ? -1 : 1;
        }
        
        if (isZLeg && isNeg && current == 0) {
            if (!insideBorder) {
                paused = true;
                
                world.save();
                cancel();
                
                Bukkit.getOnlinePlayers().forEach(online -> PacketUtils.getInstance().sendAction(online, getMessage(world, 100)));
                Bukkit.getLogger().info(PregenManager.PREFIX + getMessage(world, 100));;
                
                Bukkit.getPluginManager().callEvent(new PregenFinishEvent(world, reportTotal));
                return false;
            } else {
                insideBorder = false;
            }
        }
        
        return true;
    }

    /**
     * Check if the given X and Z are inside the pregen border.
     * 
     * @param xLoc The X loc to use.
     * @param zLoc The Z loc to use.
     * 
     * @return True if it is, false otherwise.
     */
    public boolean isInsideBorder(double xLoc, double zLoc) {
        return !(xLoc < minX || xLoc > maxX || zLoc < minZ || zLoc > maxZ);
    }
    
    /**
     * Get the current available memory in a int form.
     * 
     * @return Availalble memory.
     */
    private int getAvailableMemory() {
        return (int) (((runtime.maxMemory() - runtime.totalMemory()) + runtime.freeMemory()) / 1048576);
    }

    /**
     * Cancel the pregen task.
     */
    @Override
    public void cancel() {
        try {
            plugin.setGameState(GameState.LOBBY);
            super.cancel();
        } catch (Exception e) {
            return;
        }
        
        Bukkit.getPluginManager().callEvent(new PregenFinishEvent(Bukkit.getWorld(worldName), reportTotal));
    }

    /**
     * Toggle the paused state of the pregen task.
     */
    public void pause() {
        if (pausedForMemory) {
            pause(false);
        }
        else if (pausedForTPS) {
            pause(false);
        }
        else {
            pause(!paused);
        }
    }

    /**
     * Set the paused state of the pregen task to the given state.
     * <p>
     * If the given state is false, paused for memory & tps will be unpaused.
     * 
     * @param pause The new paused state.
     */
    public void pause(boolean pause) {
        if (pausedForMemory && !pause) {
            pausedForMemory = false;
        }
        else if (pausedForTPS && !pause) {
            pausedForTPS = false;
        }
        else {
            paused = pause;
        }
        
        if (paused) {
            storeSettings();
        } else {
            unstoreSettings();
        }
    }

    /**
     * Check if the task is currently paused.
     * 
     * @return True if it is, false otherwise.
     */
    public boolean isPaused() {
        return paused || pausedForMemory || pausedForTPS;
    }

    /**
     * Store all the pregen task settings to the data config.
     */
    private void storeSettings() {
        ConfigManager.getData().set("pregen task.world", worldName);
        ConfigManager.getData().set("pregen task.radius", radius);
        ConfigManager.getData().set("pregen task.chunksPerRun", chunksPerRun);
        ConfigManager.getData().set("pregen task.tickFrequency", tickFrequency);
        ConfigManager.getData().set("pregen task.x", refX);
        ConfigManager.getData().set("pregen task.z", refZ);
        ConfigManager.getData().set("pregen task.length", refLength);
        ConfigManager.getData().set("pregen task.total", refTotal);
        ConfigManager.getData().set("pregen task.forceLoad", forceLoad);
        ConfigManager.getInstance().saveData();
    }

    /**
     * Remove the storage of the pregen task settings.
     */
    private void unstoreSettings() {
        ConfigManager.getData().set("pregen task", null);
        ConfigManager.getInstance().saveData();
    }
}