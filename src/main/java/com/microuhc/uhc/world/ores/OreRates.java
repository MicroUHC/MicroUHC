package com.microuhc.uhc.world.ores;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.microuhc.uhc.config.ConfigManager;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.microuhc.uhc.events.other.ChunkModifiableEvent;
import com.microuhc.uhc.util.BlockUtils;

public class OreRates implements Listener {

    protected static final Collection<Material> LIMITED_ORES = ImmutableList.of(
            Material.GOLD_ORE,
            Material.DIAMOND_ORE,
            Material.REDSTONE_ORE
    );

    public enum Type {
        NO_CHANGE(0.0d, "§aVanilla", ImmutableList.of("§7Vanilla 1.8 ore rates."),
                "§aVanilla.", ImmutableList.of()),
        MINORLY_LIMITED(0.2d, "§eMinorly limited. §7(Less veins)", ImmutableList.of(
                "§e20% §7of gold, diamond and",
                "§7redstone veins are §eremoved§7.",
                "",
                "§cNote: §7This displays as \"§aVanilla§7\"",
                "§7in the §e/uhc §7command."
        ), "§aVanilla.", ImmutableList.of()),
        LIMITED_LESS_VEINS(0.5d, "§eLimited. §7(Less veins)", "§e50% §7of gold, diamond and", "§7redstone veins are §eremoved§7."),
        LIMITED_SMALLER_VEINS(0.0d, "§eLimited. §7(Smaller veins)", "§7Gold, diamond and redstone", "§7spawn in veins of §e6 or less", "§7(instead of 8 or less)."),
        DOUBLED(0.0d, "§6Doubled.", ImmutableList.of(
                "§7Ores are twice as common.",
                "§4§lHOST THIS ONLY IN",
                "§4§l30 MINUTE GAMES!"
        ), "§6Doubled.", ImmutableList.of());

        private final double oreRemovalChance;

        private final String worldCreatorGUIDescription;
        private final List<String> worldCreatorGUILore;

        private final String gameInfoGUIDescription;
        private final List<String> gameInfoGUILore;

        Type(double oreRemovalChance, String description, String... lore) {
            this(oreRemovalChance, description, ImmutableList.copyOf(lore));
        }

        Type(double oreRemovalChance, String description, List<String> lore) {
            this(oreRemovalChance, description.replace(".", ""), lore, description, lore);
        }

        Type(double oreRemovalChance, String worldCreatorGUIDescription, List<String> worldCreatorGUILore, String gameInfoGUIDescription, List<String> gameInfoGUILore) {
            this.oreRemovalChance = oreRemovalChance;
            this.worldCreatorGUIDescription = worldCreatorGUIDescription;
            this.worldCreatorGUILore = worldCreatorGUILore;
            this.gameInfoGUIDescription = gameInfoGUIDescription;
            this.gameInfoGUILore = gameInfoGUILore;
        }

        public double getOreRemovalChance() {
            return oreRemovalChance;
        }

        public String getWorldCreatorGUIDescription() {
            return worldCreatorGUIDescription;
        }

        public List<String> getWorldCreatorGUILore() {
            return worldCreatorGUILore;
        }

        public String getGameInfoGUIDescription() {
            return gameInfoGUIDescription;
        }

        public List<String> getGameInfoGUILore() {
            return gameInfoGUILore;
        }
    }

    private final Random random = new Random();
    private final ConfigManager configManager;

    public OreRates(ConfigManager configManager) {
        this.configManager = configManager;
    }

    @EventHandler
    public void on(ChunkModifiableEvent event) {
        Type oreRate = Type.valueOf(configManager.getWorlds().getString(
                event.getWorld().getName() + ".oreRates", Type.NO_CHANGE.name()));

        if (oreRate.getOreRemovalChance() <= 0.0d) {
            return;
        }

        Chunk chunk = event.getChunk();
        Set<Block> checked = Sets.newHashSet();

        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 32; y++) {
                for (int z = 0; z < 16; z++) {
                    Block block = chunk.getBlock(x, y, z);

                    if (checked.contains(block)) {
                        continue;
                    }

                    Material type = block.getType();

                    if (!LIMITED_ORES.contains(type)) {
                        continue;
                    }

                    List<Block> vein = BlockUtils.getVein(block);
                    checked.addAll(vein);

                    if (random.nextDouble() > oreRate.getOreRemovalChance()) {
                        continue;
                    }

                    vein.forEach(veinBlock -> veinBlock.setType(Material.STONE));
                }
            }
        }
    }

}
