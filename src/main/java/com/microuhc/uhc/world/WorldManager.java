package com.microuhc.uhc.world;

import java.util.Map;
import java.util.Set;

import com.microuhc.uhc.config.ConfigManager;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldBorder;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.microuhc.uhc.exceptions.CommandException;
import com.microuhc.uhc.util.FileUtils;
import com.microuhc.uhc.util.LocationUtils;
import com.microuhc.uhc.world.ores.OreRates;

public class WorldManager {

    private ConfigManager configManager;

    public WorldManager(ConfigManager configManager) {
        this.configManager = configManager;
    }

    /**
     * Load all the saved worlds.
     */
    public void loadWorlds() {
        final Set<String> worlds = configManager.getWorlds().getKeys(false);

        if (worlds == null) {
            return;
        }

        for (String world : worlds) {
            try {
                loadWorld(world);
            } catch (Exception ex) {
                Bukkit.getLogger().severe(ex.getMessage());
            }
        }
    }

    /**
     * Create a world with the given settings.
     *
     * @param name The world name.
     * @param diameter The world diameter size.
     * @param seed The world seed.
     * @param environment The world environment.
     * @param type The world type.
     * @param antiStripmine Wether antistripmine should be enabled.
     * @param oreRates The ore type.
     * @param newStone Wether the world should have the new 1.8 stone.
     * @param centerX The world X center.
     * @param centerZ The world Z center.
     */
    public void createWorld(String name, int diameter, long seed, Environment environment, WorldType type, boolean antiStripmine, OreRates.Type oreRates, boolean newStone, double centerX, double centerZ) {
        configManager.getWorlds().set(name + ".name", name);
        configManager.getWorlds().set(name + ".radius", diameter);
        configManager.getWorlds().set(name + ".seed", seed);

        configManager.getWorlds().set(name + ".environment", environment.name());
        configManager.getWorlds().set(name + ".worldtype", type.name());
        configManager.getWorlds().set(name + ".diameter", diameter);

        configManager.getWorlds().set(name + ".antiStripmine", antiStripmine);
        configManager.getWorlds().set(name + ".oreRates", oreRates.name());
        configManager.getWorlds().set(name + ".newStone", newStone);
        configManager.getWorlds().set(name + ".center.x", centerX);
        configManager.getWorlds().set(name + ".center.z", centerZ);

        configManager.saveWorlds();

        World world;

        try {
            world = loadWorld(name);
        } catch (CommandException e) {
            throw new AssertionError(e);
        }

        world.setGameRuleValue("doDaylightCycle", "false");
        world.setSpawnFlags(false, true);

        int y = LocationUtils.highestTeleportableYAtLocation(new Location(world, centerX, 0, centerX)) + 2;
        world.setSpawnLocation((int) centerX, y, (int) centerZ);

        WorldBorder border = world.getWorldBorder();
        border.setSize(diameter);
        border.setWarningDistance(0);
        border.setWarningTime(60);
        border.setDamageAmount(0.1);
        border.setDamageBuffer(0);
        border.setCenter(centerX, centerZ);
    }

    /**
     * Delete the given world.
     *
     * @param world The world deleting.
     * @return True if it was deleted, false otherwise.
     */
    public boolean deleteWorld(World world) {
        for (Player player : world.getPlayers()) {
            player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        }

        Bukkit.unloadWorld(world, true);

        configManager.getWorlds().set(world.getName(), null);
        configManager.saveWorlds();

        return FileUtils.deleteFile(world.getWorldFolder());
    }

    /**
     * Loads the world with the given world name.
     *
     * @param name The name of the world.
     * @throws CommandException If the world name isn't a existing world.
     */
    public World loadWorld(String name) throws CommandException {
        Set<String> worlds = configManager.getWorlds().getKeys(false);

        if (!worlds.contains(name)) {
            throw new CommandException("The world '" + name + "' does not exist.");
        }

        Environment environment = Environment.valueOf(configManager.getWorlds().getString(name + ".environment", Environment.NORMAL.name()));
        WorldType type = WorldType.valueOf(configManager.getWorlds().getString(name + ".worldtype", WorldType.NORMAL.name()));
        OreRates.Type oreRates = OreRates.Type.valueOf(configManager.getWorlds().getString(name + ".oreRates", OreRates.Type.NO_CHANGE.name()));
        long seed = configManager.getWorlds().getLong(name + ".seed", 2347862349786234l);
        boolean newStone = configManager.getWorlds().getBoolean(name + ".newStone", true);

        WorldCreator creator = new WorldCreator(name);
        creator.generateStructures(true);
        creator.environment(environment);
        creator.type(type);
        creator.seed(seed);
        creator.generatorSettings(getGeneratorSettings(type, newStone, oreRates));

        World world = creator.createWorld();
        world.setDifficulty(Difficulty.HARD);

        world.save();
        return world;
    }

    protected String getGeneratorSettings(WorldType worldtype, boolean newStone, OreRates.Type oreRates) {
        if (worldtype == WorldType.FLAT) {
            return "3;minecraft:bedrock,2*minecraft:dirt,minecraft:grass;1;village(size=65535 distance=9)";
        }

        Map<String, Object> generatorSettings = Maps.newHashMap();
        generatorSettings.put("useMonuments", false);

        if (!newStone) {
            for (String stoneName : ImmutableList.of("granite", "diorite", "andesite")) {
                generatorSettings.put(stoneName + "Size", 1);
                generatorSettings.put(stoneName + "Count", 0);
                generatorSettings.put(stoneName + "MinHeight", 0);
                generatorSettings.put(stoneName + "MaxHeight", 0);
            }
        }

        if (oreRates == OreRates.Type.LIMITED_SMALLER_VEINS) {
            generatorSettings.put("goldSize", 6);
            generatorSettings.put("redstoneSize", 6);
            generatorSettings.put("diamondSize", 6);
        }

        if (oreRates == OreRates.Type.DOUBLED) {
            generatorSettings.put("coalCount", 40);
            generatorSettings.put("ironCount", 40);
            generatorSettings.put("goldCount", 4);
            generatorSettings.put("redstoneCount", 16);
            generatorSettings.put("diamondCount", 2);
            generatorSettings.put("lapisCount", 2);
        }

        return new Gson().toJson(generatorSettings);
    }

    /**
     * Unloads the given world.
     *
     * @param world The world.
     * @return True if successful, false otherwise.
     */
    public boolean unloadWorld(World world) {
        for (Player player : world.getPlayers()) {
            player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        }

        return Bukkit.unloadWorld(world, true);
    }

}
