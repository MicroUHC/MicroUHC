package com.microuhc.uhc.gui.guis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.microuhc.uhc.game.GameRole;
import com.microuhc.uhc.user.User;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.gui.GUI;
import com.microuhc.uhc.util.PlayerUtils;

public class SetRoleGUI extends GUI implements Listener {

    private MicroUHC uhc;

    /**
     * SetRole GUI class constructor.
     *
     */
    public SetRoleGUI(MicroUHC uhc) {
        super("Set Role", "A inventory with options for setting the users role");

        this.uhc = uhc;
    }

    private final Inventory inv = Bukkit.createInventory(null, 9, ChatColor.RED + "Change role");

    @Override
    public void onSetup() {
    }

    /**
     * Get the stats inventory for the given user.
     *
     * @param user The stats owner.
     * @return The inventory.
     */
    public Inventory get(User user) {
        update(user, inv);
        return inv;
    }

    @EventHandler
    public void on(InventoryClickEvent e) {
        if(e.getView().getTopInventory() != null && e.getWhoClicked() instanceof Player){
            Player player = (Player) e.getWhoClicked();
            User user = uhc.getUserMap().get(player.getUniqueId());
            int slot = e.getRawSlot();
            int gameroleid = (slot - 2) / 2;
            if(gameroleid >= 0 && gameroleid < GameRole.values().length-1){
                GameRole gameRole = GameRole.getRole(gameroleid);
                if(user.getRole() == gameRole){
                    user.play(Sound.ENTITY_BLAZE_HURT);
                    user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "You are already a " + gameRole.getPrefix());
                }
                else if(user.canChangeRole(gameRole)){
                    user.play(Sound.ENTITY_PLAYER_LEVELUP);
                    user.setRole(gameRole);
                    user.sendBossBar(uhc, BarColor.BLUE, ChatColor.GRAY + "You are now a " + gameRole.getPrefix());
                    //update(user, e.getView().getTopInventory());
                    update(user, e.getView().getTopInventory());
                }
                else{
                    user.play(Sound.ENTITY_BLAZE_HURT);
                    user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "You do not have permission for this role");
                }

            }
            e.setCancelled(true);
        }
    }

    /**
     * Update the GUI items.
     */
    public void update(User user, Inventory inventory) {
        int i = 2;
        for(GameRole gameRole : GameRole.values()){
            if(gameRole != GameRole.HOST) {
                ItemStack itemStack = new ItemStack(Material.STAINED_GLASS_PANE);
                ItemMeta meta = itemStack.getItemMeta();
                String display;
                List<String> lore = Arrays.asList(ChatColor.GRAY + gameRole.getDescription());
                meta.setLore(lore);
                if (user.getRole() == gameRole) {
                    itemStack.setDurability(DyeColor.BLUE.getData());
                    display = ChatColor.BLUE + "You are currently a " + gameRole.getPrefix();
                } else if (user.canChangeRole(gameRole)) {
                    itemStack.setDurability(DyeColor.GREEN.getData());
                    display = ChatColor.GREEN + "You have permission for a " + gameRole.getPrefix();
                } else {
                    itemStack.setDurability(DyeColor.RED.getData());
                    display = ChatColor.RED + "You cannot become a " + gameRole.getPrefix();
                }
                meta.setDisplayName(display);
                itemStack.setItemMeta(meta);
                inventory.setItem(i, itemStack);
                i+= 2;
            }
        }
    }

}
