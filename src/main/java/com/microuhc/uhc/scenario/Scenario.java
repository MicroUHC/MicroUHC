package com.microuhc.uhc.scenario;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.commands.BasicCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.Collections;

public abstract class Scenario implements Listener{
    private final MicroUHC uhc;
    private final ScenarioManager scenarioManager;
    private final String name, description;

    public Scenario(MicroUHC uhc, ScenarioManager scenarioManager, String name, String description) {
        this.uhc = uhc;
        this.scenarioManager = scenarioManager;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled(){
        return uhc.getScenarioManager().getScenarioList().contains(this);
    }

    public void init(){
        Bukkit.getPluginManager().registerEvents(this, uhc);
    }

    public void kill(){
        HandlerList.unregisterAll(this);
    }

    public String getPrefix(){
        return ChatColor.GREEN + "[" + name + "] " + ChatColor.RESET;
    }
}
