package com.microuhc.uhc.scenario;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.scenario.scenarios.CutClean;
import com.microuhc.uhc.scenario.scenarios.TripleOres;

import java.util.ArrayList;
import java.util.List;


public class ScenarioManager {
    private static final Scenario[] scenarios = {};

    public static Scenario[] getScenarios() {
        return scenarios;
    }

    private final MicroUHC uhc;
    private List<Scenario> scenarioList = new ArrayList<>();

    public final CutClean CUT_CLEAN;
    public final TripleOres TRIPLE_ORES;

    public ScenarioManager(MicroUHC uhc) {
        this.uhc = uhc;
        CUT_CLEAN = new CutClean(uhc, this);
        TRIPLE_ORES = new TripleOres(uhc, this);
    }

    public void loadScenarios(List<String> scenarios){
        scenarioList = new ArrayList<>();
        for(Scenario scenario: ScenarioManager.scenarios){
            if(scenarios.contains(scenario.getName())){
                scenarioList.add(scenario);
            }
        }
    }

    public boolean isEnabled(Scenario scenario){
        return scenarioList.contains(scenario);
    }

    public void init(){
        for(Scenario scenario: scenarioList){
            scenario.init();
        }
    }

    public void kill(){
        for(Scenario scenario: scenarioList){
            scenario.kill();
        }
    }

    public List<Scenario> getScenarioList() {
        return scenarioList;
    }
}
