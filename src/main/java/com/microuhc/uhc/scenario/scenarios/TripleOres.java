package com.microuhc.uhc.scenario.scenarios;

import com.microuhc.uhc.MicroUHC;
import com.microuhc.uhc.scenario.Scenario;
import com.microuhc.uhc.scenario.ScenarioManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class TripleOres extends Scenario{
    private final MicroUHC uhc;
    private final ScenarioManager scenarioManager;

    public TripleOres(MicroUHC uhc, ScenarioManager scenarioManager) {
        super(uhc, scenarioManager, "TripleOres", "Get triple the amount of loot from every ore you mine");
        this.uhc = uhc;
        this.scenarioManager = scenarioManager;
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public void kill() {
        super.kill();
    }

    @EventHandler
    public void onPlayerMine(BlockBreakEvent e){
        List<ItemStack> toDrop = new ArrayList<>(e.getBlock().getDrops(e.getPlayer().getInventory().getItemInMainHand()));
    }
}
