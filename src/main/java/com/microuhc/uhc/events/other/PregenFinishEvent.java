package com.microuhc.uhc.events.other;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.World;

/**
 * Pregen finish event class.
 * 
 * @author LeonTG77
 */
public class PregenFinishEvent extends Event {
    private final long totalChunks;
    private final World world;

    public PregenFinishEvent(World world, long totalChunks) {
        this.totalChunks = totalChunks;
        this.world = world;
    }

    /**
     * Get the world that was pregenned.
     * 
     * @return The world.
     */
    public World getWorld() {
        return world;
    }

    /**
     * Get the total amount of chunks that were pregenned.
     * 
     * @return The amount of chunks.
     */
    public long getTotalChunks() {
        return totalChunks;
    }
    
    private static final HandlerList HANDLERS = new HandlerList();
    
    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }
     
    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
