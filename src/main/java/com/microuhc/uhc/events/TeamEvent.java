package com.microuhc.uhc.events;

import org.bukkit.OfflinePlayer;
import org.bukkit.event.Event;
import org.bukkit.scoreboard.Team;

/**
 * Team join event class.
 * 
 * @author LeonTG77
 */
public abstract class TeamEvent extends Event {
    private final OfflinePlayer player;
    private final Team team;

    public TeamEvent(Team team, OfflinePlayer player) {
        this.player = player;
        this.team = team;
    }

    /**
     * Get the player that joined the team.
     * 
     * @return The player.
     */
    public OfflinePlayer getPlayer() {
        return player;
    }
    
    /**
     * Get the team the player joined.
     * 
     * @return The team.
     */
    public Team getTeam() {
        return team;
    }
}