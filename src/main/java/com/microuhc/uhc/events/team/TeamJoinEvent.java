package com.microuhc.uhc.events.team;

import org.bukkit.OfflinePlayer;
import org.bukkit.event.HandlerList;
import org.bukkit.scoreboard.Team;

import com.microuhc.uhc.events.TeamEvent;

/**
 * Team join event class.
 * 
 * @author LeonTG77
 */
public class TeamJoinEvent extends TeamEvent {

    public TeamJoinEvent(Team team, OfflinePlayer player) {
        super(team, player);
    }
    
    private static final HandlerList HANDLERS = new HandlerList();
    
    public HandlerList getHandlers() {
        return HANDLERS;
    }
     
    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}